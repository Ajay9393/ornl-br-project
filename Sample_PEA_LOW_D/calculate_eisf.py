# -*- coding: utf-8 -*-
"""
Created on Thu Feb 21 17:51:26 2019

@author: ak4jo
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import re
import pickle
from scipy.special import jv
from scipy.optimize import curve_fit

def fit_multiple_polys(list_of_functions):
    def sum_polys(x, *n1):
    ##n1 will be all fractions for MA rotations and all fraactions for FA rotations, in the order corresponding
    ##to the flattened array of polys passed (i.e all MA then all FA)
        
        
        PEA_EISF = list_of_functions[0]
        num_fcns = len(PEA_EISF)
        
        temp = np.zeros(len(x))
        n1 = np.array(n1).ravel()
        for idx, fcn in enumerate(PEA_EISF):
            temp = temp + (1.-n1[idx]*(1.-fcn(x)))/(num_fcns)
        
        return temp
    return sum_polys

def generate_general_EISF(q, distances_H,order):
    try:
        n_rotations, n_hydrogens = distances_H.shape
        print n_rotations, n_hydrogens
    except ValueError:
        n_rotations = 1
        n_hydrogens = distances_H.shape[0]
    
    q = q.reshape(len(q),1)
    j_tot = np.zeros([len(q)])
    
    for rot in range(0, n_rotations):
        j_temp = np.average(jv(0,q*distances_H[rot].reshape(1,n_hydrogens)),axis = 1)
        j_tot = j_tot+j_temp
    return j_tot/float(order)

distances_pi_rot = np.load('pi_distances.npy') 
elements = pickle.load(open('labels.pickle','rb'))
q = np.linspace(0,5,num=1000)
h_distances_pi_rot = np.array([x for idx,x in enumerate(distances_pi_rot) if elements[idx] == 'H'])
E_rotation = np.zeros(h_distances_pi_rot.shape)
C2_EISF = generate_general_EISF(q, np.array([E_rotation,h_distances_pi_rot]), 2)

f, ax = plt.subplots(nrows =1, ncols = 1, figsize = (14,7))
f.subplots_adjust(wspace = 0.4)
ax.plot(q,C2_EISF,c='k', label = '$C_2$ EISF')

EISF_poly_C2 = np.poly1d(np.polyfit(q, C2_EISF,15))
PEA_poly_fcns = {'C2':EISF_poly_C2}

ax.plot(q, EISF_poly_C2(q), c = 'k', linestyle = '-.', label = 'Poly Fit')
ax.set_xlabel('Q $(A^{-1})$')
ax.set_ylabel('A$_Q$')
ax.set_title('$C_8H_{12}$ EISF')
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles,labels)

EISF = pickle.load(open(r'../EISF.pickle','rb'))

PEA_to_fit  = {x:EISF[x] for x in EISF.keys() if re.findall('PEA_LOW_D/n=1',x)}

color = cm.nipy_spectral(np.linspace(0,0.8,len(PEA_poly_fcns.keys())))
dict_of_fits = {}

list_of_poly_fcns = [['C2']]

q = np.linspace(0,2,num=1000)

f2,ax2 = plt.subplots(nrows = len(PEA_to_fit)/2, ncols = 2,figsize = (20,40))
f2.subplots_adjust(wspace = 0.4, top = 1)
f2.tight_layout(rect=[0, 0.03, 1, 0.95])
for idx1, key in enumerate(PEA_to_fit.keys()):
    dict_of_fits[key] = {}
    q_val = EISF[key][0][:-1]
    val = EISF[key][1][:-1]
    ax2[idx1 / 2, idx1 % 2].scatter(q_val[:-1], val[:-1], color = 'k', label = 'Experimental Data')

    for idx2, combo in enumerate(list_of_poly_fcns):
        identifier = '+'.join(combo)
        fcn = [PEA_poly_fcns[x] for x in combo]
        bounds = (np.zeros(len(fcn)), np.ones(len(fcn)))
        p0 = np.array([0.03]*len(fcn))
    
        pp1,pp2 = curve_fit(fit_multiple_polys([fcn]),xdata = q_val[:-1],ydata = val[:-1], p0 =p0, bounds = bounds, method = 'dogbox')
        res = val[:-1] - fit_multiple_polys([fcn])(q_val[:-1], pp1)
        ss_res = np.sum(res**2)
        ss_tot = np.sum((val[:-1] - np.mean(val[:-1]))**2) 
        r_squared = 1. - (ss_res/ss_tot)
        
        
        ax2[idx1 /2, idx1%2].plot(q, fit_multiple_polys([fcn])(q, pp1), color = color[idx2], \
                       linestyle = '-.', label = '+'.join(combo) + ': $R^{2}$ = ' + str(round(r_squared,3))\
                       + ', $x_1$ = ' + ','.join([str(round(x,2)) for x in pp1.tolist()]))

        

        dict_of_fits[key][identifier] =  [pp1, r_squared]
    
    handles, labels = ax2[idx1/2, idx1%2].get_legend_handles_labels()
    ax2[idx1 / 2, idx1 % 2].legend(handles, labels)#    
    ax2[idx1 / 2, idx1 % 2].set_title(key)





