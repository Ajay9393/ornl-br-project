# -*- coding: utf-8 -*-
"""
Created on Sun Feb 10 11:11:33 2019

@author: ak4jo
"""
import re
import numpy as np
import pickle
def get_center_mass(center, positions, masses):
    return np.sum(masses*(positions - center), axis = 0)
    
def apply_rotation(R, data_points):
    ##takes data_points which are list of positions i.e [[x,y,z],[x,y,x]]
    return np.transpose(np.dot(R,np.transpose(data_points)))
    
def generate_rotation_about_axis(V, theta):
    ##Takes a unit vector and an angle and returns the matrix rotating points about an axis described by that unit vector
    M = np.array(\
                [[V[0]**2.0 + (V[1]**2.0+V[2]**2.0)*np.cos(theta),\
                  V[0]*V[1]*(1-np.cos(theta)) - V[2]*np.sin(theta), \
                  V[0]*V[2]*(1-np.cos(theta))+V[1]*np.sin(theta)],\
                [V[0]*V[1]*(1- np.cos(theta))+V[2]*np.sin(theta),\
                 V[1]**2.0 + (V[0]**2.0+V[2]**2.0)*np.cos(theta),\
                 V[1]*V[2]*(1-np.cos(theta)) - V[0]*np.sin(theta)],
                 [V[0]*V[2]*(1-np.cos(theta)) - V[1]*np.sin(theta),\
                  V[1]*V[2]*(1-np.cos(theta)) + V[0]*np.sin(theta),\
                  V[2]**2.0+(V[0]**2.0+V[1]**2.0)*np.cos(theta)]])
    return M
                 

def generate_rotation_vector(a,b):
    ##generate matrix to rotate from a to b
    v = np.cross(a, b)
    c = np.dot(a, b)
    G = np.array([[c,-np.linalg.norm(v),0],[np.linalg.norm(v),c,0],[0,0,1]])
    x_1 = np.dot(a,b)*a/(np.linalg.norm(np.dot(a,b)*a))
    x_2 = (b - np.dot(a,b)*a)/(np.linalg.norm(b-np.dot(a,b)*a))
    x_3 = np.cross(b,a)
    F = np.linalg.inv(np.transpose(np.array([x_1,x_2,x_3])))
    invF = np.linalg.inv(F)
    R = np.dot(invF,np.dot(G,F))
    return R

def annotate2D(ax,labels, datapoints):
    ##Labels the points of a 2D plot
    for label, x,y in zip(labels, datapoints[:,0],datapoints[:,1]):
        ax.annotate(
        label,
        xy=(x, y), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
    
def annotate3D(ax,labels,datapoints):
    for idx,label in enumerate(labels):
        x2,y2,_ = proj3d.proj_transform(datapoints[idx,0],datapoints[idx,1],datapoints[idx,2],ax.get_proj())    
        ax.annotate(
            label,
            xy=(x2, y2), xytext=(-20, 20),
            textcoords='offset points', ha='right', va='bottom',
            bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
            arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))


def generate_general_EISF(q, distances_H,order):
    try:
        n_rotations, n_hydrogens = distances_H.shape
        print n_rotations, n_hydrogens
    except ValueError:
        n_rotations = 1
        n_hydrogens = distances_H.shape[0]
    
    q = q.reshape(len(q),1)
    j_tot = np.zeros([len(q)])
    
    for rot in range(0, n_rotations):
        j_temp = np.average(jv(0,q*distances_H[rot].reshape(1,n_hydrogens)),axis = 1)
        j_tot = j_tot+j_temp
    return j_tot/float(order)

with open('PEA.mol','rb') as f:
    Data = f.read().splitlines()
    
num_elements = 21

flag = re.compile(r'\s+')
x_pos = [float(flag.split(y)[1]) for y in Data[4:4+num_elements]]
y_pos = [float(flag.split(y)[2]) for y in Data[4:4+num_elements]]
z_pos = [float(flag.split(y)[3]) for y in Data[4:4+num_elements]]


elements = [flag.split(y)[4] for y in Data[4:4+num_elements]]


data_points = np.array([[x_pos[idx], y_pos[idx],z_pos[idx]] for idx, ele in enumerate(elements)])

pickle.dump(elements, open('labels.pickle','wb'))
np.save('PEApos.npy',data_points)


##align C-N bond to [100] axis

unit_C_N = -1.0/np.linalg.norm(data_points[0] - data_points[2])*(data_points[0] - data_points[2])
R = generate_rotation_vector(unit_C_N,np.array([0,0,1], dtype = float))
data_points = apply_rotation(R,data_points)

np.save('align_to_z.npy',data_points)


##align ring to be in parallel to y-z plane
unit_C_C = 1.0/np.linalg.norm(data_points[4] - data_points[5])*(data_points[4] - data_points[5])
R = generate_rotation_vector(unit_C_C,np.array([1,0,0], dtype = float))
data_points = apply_rotation(R,data_points)

np.save('align_ring.npy',data_points)



R_pi_2= generate_rotation_about_axis([0,0,1], np.pi/2.0)

data_points_pi_2 = apply_rotation(R_pi_2,data_points)

np.save('rotate_pi_2.npy',data_points_pi_2)

R_pi= generate_rotation_about_axis([0,0,1], np.pi)

data_points_pi = apply_rotation(R_pi,data_points)

np.save('rotate_pi.npy',data_points_pi)

pi_d = np.linalg.norm(data_points_pi - data_points, axis = 1)

np.save('pi_distances.npy',pi_d)






