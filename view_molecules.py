# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
from ase import Atoms
from ase.visualize import view
import pickle


molecule = 'MA'

if molecule == 'MA':
    N = np.array([0.06960,0.43040,0.28580])*np.array([8.87960,8.87960,12.62660])
    H1 = np.array([0.16490,0.49330,0.30000])*np.array([8.87960,8.87960,12.62660])
    H2 = np.array([0.09520,0.34390,0.23450])*np.array([8.87960,8.87960,12.62660])
    H3 = np.array([0.03040,0.38640,0.35600])*np.array([8.87960,8.87960,12.62660])
    C = np.array([-0.04660,0.52700,0.23850])*np.array([8.87960,8.87960,12.62660])
    H4 = np.array([-0.07570,0.61330,0.29250])*np.array([8.87960,8.87960,12.62660])
    H5 = np.array([-0.14330,0.46100,0.22130])*np.array([8.87960,8.87960,12.62660])
    H6 = np.array([-0.00490,0.57500,0.16760])*np.array([8.87960,8.87960,12.62660])
    
    
    elements = pickle.load(open('MA Rotations/labels.pickle','rb'))
    positions = np.load('MA Rotations/MA_positions.npy')
    
    
    
    a = Atoms(elements, positions = positions)
    
    after_rotation = np.load('MA Rotations/rotation1.npy')
    
    b = Atoms(elements,positions = after_rotation)


elif molecule == 'FA':
    C = np.array([-0.079, -0.014,-0.022])*np.array([5.9677,5.9676,5.9676])
    H1 = np.array([-0.2069,-0.1026,-0.0096])*np.array([5.9677,5.9676,5.9676])
    N1 = np.array([-0.038,0.122,0.141])*np.array([5.9677,5.9676,5.9676])
    H2 = np.array([0.0782,0.2069,0.13620])*np.array([5.9677,5.9676,5.9676])
    H3 = np.array([-0.1267,0.12760,0.25440])*np.array([5.9677,5.9676,5.9676])
    N2 = np.array([0.04,-0.038,-0.201])*np.array([5.9677,5.9676,5.9676])
    H4 = np.array([0.1596,0.0408,-0.2186])*np.array([5.9677,5.9676,5.9676])
    H5 = np.array([0.0008,-0.13270,-0.3016])*np.array([5.9677,5.9676,5.9676])
    
    
    a = Atoms(['C','H','N','H','H','N','H','H'], positions = np.array([C,H1,N1,H2,H3,N2,H4,H5]))
    
    after_rotation = np.load('FA Rotations/rotation1.npy')
    
    b = Atoms(['C','H','N','H','H','N','H','H'],positions = after_rotation)
    
    after_rotation_2 =np.load('FA Rotations/rotation2.npy')
    
    c = Atoms(['C','H','N','H','H','N','H','H'],positions = after_rotation_2)


#PEA
elif molecule == 'PEA':
    elements = pickle.load(open('Sample_PEA_LOW_D/labels.pickle','rb'))
    positions = np.load('Sample_PEA_LOW_D/PEApos.npy')
    a = Atoms(elements, positions = positions)
    align = np.load('Sample_PEA_LOW_D/align_to_z.npy')
    b = Atoms(elements, positions = align)
    align_2 = np.load('Sample_PEA_LOW_D/align_ring.npy')
    b_2 = Atoms(elements, positions = align_2)
    pi_2 = np.load('Sample_PEA_LOW_D/rotate_pi_2.npy')
    c = Atoms(elements, positions = pi_2)
    pi = np.load('Sample_PEA_LOW_D/rotate_pi.npy')
    d = Atoms(elements, positions = pi)
#    view(a)
#    view(b)
#    view(b_2)
#    view(c)
#    view(d)
    
elif molecule =='BTA':
    elements = pickle.load(open('Sample_BTA_LOW_D/labels.pickle','rb'))
    positions = np.load('Sample_BTA_LOW_D/BTApos.npy')
    a = Atoms(elements, positions = positions)
    align = np.load('Sample_BTA_LOW_D/align_to_z.npy')
    b = Atoms(elements, positions = align)
    align_2 = np.load('Sample_BTA_LOW_D/align_H.npy')
    b_2 = Atoms(elements, positions = align_2)
#    pi_2 = np.load('Sample_PEA_LOW_D/rotate_pi_2.npy')
#    c = Atoms(elements, positions = pi_2)
#    pi = np.load('Sample_PEA_LOW_D/rotate_pi.npy')
#    d = Atoms(elements, positions = pi)
    view(a) 
    view(b)
    view(b_2)

