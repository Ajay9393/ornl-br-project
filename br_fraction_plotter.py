# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 21:52:37 2019

@author: ak4jo
"""
import matplotlib.pyplot as plt
import numpy as np

br = [0, 0.1,0.15,0.2]

x = np.array([0.85,0.64,0.56,0.65])
pce = [11.5,14.2,18.5,16.5]

f, ax1 = plt.subplots()
ax1.plot(br,1.- x,'-o', color = 'b')
ax1.set_title('Formamidinium Rotation')
ax1.set_xlabel('Fraction of Bromine Incorporated')
ax1.set_ylabel('Immobile H Fraction', color = 'b')
ax1.tick_params(axis='y', labelcolor='b')


ax2 = ax1.twinx()

ax2.plot(br,pce, '-o',color = 'r')
ax2.tick_params(axis='y', labelcolor='r')

ax2.set_ylabel('Power Conversion Efficiency (%)', color = 'r')


plt.show()