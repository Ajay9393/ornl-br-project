# -*- coding: utf-8 -*-
"""
Created on Wed Jan 09 13:47:58 2019

@author: ak4jo
"""


import numpy as np
import matplotlib.pyplot as plt
import re
import pickle
from scipy.special import jv
import sys
sys.path.insert(0, '../../../')

wd = 'Figures/'
import DataAnalysis as aj

def generate_C2_EISF(q, distances_H):
    n_hydrogens = distances_H.shape[0]
    q = q.reshape(len(q),1)

    j_C2 = np.sum(jv(0, q * distances_H.reshape(1, n_hydrogens)), axis = 1)/float(n_hydrogens)

    return (1.0+j_C2)/2.0

def generate_C6_EISF(q, distances_H):
    n_rotations,n_hydrogens = distances_H.shape
    q = q.reshape(len(q),1)

    j_C6 = np.sum(jv(0, q * distances_H[0].reshape(1, n_hydrogens)), axis = 1)/float(n_hydrogens)
    j_C2 = np.sum(jv(0, q * distances_H[1].reshape(1, n_hydrogens)), axis = 1)/float(n_hydrogens)
    j_3C6 = np.sum(jv(0, q * distances_H[2].reshape(1, n_hydrogens)), axis = 1)/float(n_hydrogens)

    return (1.0+j_C6+j_C2+j_3C6)/4.0

def generate_general_EISF(q, distances_H,order):
    try:
        n_rotations, n_hydrogens = distances_H.shape
        print n_rotations, n_hydrogens
    except ValueError:
        n_rotations = 1
        n_hydrogens = distances_H.shape[0]

    q = q.reshape(len(q),1)
    j_tot = np.zeros([len(q)])

    for rot in range(0, n_rotations):
        j_temp = np.average(jv(0,q*distances_H[rot].reshape(1,n_hydrogens)),axis = 1)
        j_tot = j_tot+j_temp
    return j_tot/float(order)



##For the C2 rotation about carbon
#H_distances_C2_rot = np.array([x for idx,x in enumerate(distances_C2_rot) if re.findall('H',labels[idx])])

##For C6 rotation about either C, N-N or center of mass
# H_distances_C_C6 = distances_C[0]
# H_distances_C_C2 = np.array([x for idx,x in enumerate(distances_C[1]) if re.findall('H',labels[idx])])
# H_distances_C_3C6 = np.array([x for idx,x in enumerate(distances_C[2]) if re.findall('H',labels[idx])])
#
# H_distances_N_C6 = np.array([x for idx,x in enumerate(distances_N[0]) if re.findall('H',labels[idx])])
# H_distances_N_C2 = np.array([x for idx,x in enumerate(distances_N[1]) if re.findall('H',labels[idx])])
# H_distances_N_3C6 = np.array([x for idx,x in enumerate(distances_N[2]) if re.findall('H',labels[idx])])
#
# H_distances_cen_C6 = np.array([x for idx,x in enumerate(distances_cen[0]) if re.findall('H',labels[idx])])
# H_distances_cen_C2 = np.array([x for idx,x in enumerate(distances_cen[1]) if re.findall('H',labels[idx])])
# H_distances_cen_3C6 = np.array([x for idx,x in enumerate(distances_cen[2]) if re.findall('H',labels[idx])])
#
# H_distances_twirl_C6 = np.array([x for idx,x in enumerate(distances_twirl[0]) if re.findall('H',labels[idx])])
# H_distances_twirl_C2 = np.array([x for idx,x in enumerate(distances_twirl[1]) if re.findall('H',labels[idx])])
# H_distances_twirl_3C6 = np.array([x for idx,x in enumerate(distances_twirl[2]) if re.findall('H',labels[idx])])

##zeros to account for identity rotation
# E_rotation = np.zeros(H_distances_C_C6.shape)

q = np.linspace(0,5,num=1000)

##Unused, use general function to get EISF now
#C_center = generate_C6_EISF(q,np.array([H_distances_C_C6,H_distances_C_C2,H_distances_C_3C6]))
#N_center = generate_C6_EISF(q,np.array([H_distances_N_C6,H_distances_N_C2,H_distances_N_3C6]))
#centermass_center = generate_C6_EISF(q,np.array([H_distances_cen_C6,H_distances_cen_C2,H_distances_cen_3C6]))
#C6_twirl = generate_C6_EISF(q,np.array([H_distances_twirl_C6,H_distances_twirl_C2,H_distances_twirl_3C6]))
#
#C2_rot = generate_C2_EISF(q, H_distances_C2_rot)


distances_C = np.load('FA Rotations/C-C-axis_C6.npy')
distances_N = np.load('FA Rotations/N-N-axis_C6.npy')
distances_cen =  np.load('FA Rotations/center_of_mass-axis_C6.npy')
distances_twirl = np.load('FA Rotations/C6_twirl_C6.npy')
labels = pickle.load(open('FA Rotations/labels.pickle','rb'))


##Needs all possible rotations, including identity
FA_C6_C_center = generate_general_EISF(q,distances_C,6)
FA_C6_N_center = generate_general_EISF(q,distances_N,6)
FA_C6_centermass_center = generate_general_EISF(q,distances_cen,6)
FA_C6_twirl = generate_general_EISF(q,np.array(distances_twirl),6)


##Get MA
labels = pickle.load(open('MA Rotations/labels.pickle','rb'))
C3_MA = np.load('MA Rotations/C3_distances.npy')
C6_MA = np.load('MA Rotations/C6_distances.npy')
C3_C6_MA = np.load('MA Rotations/C3_C6_distances.npy')

MA_C3_EISF = generate_general_EISF(q, C3_MA,3)
MA_C6_EISF = generate_general_EISF(q, C6_MA,6)
MA_C3_C6_EISF = generate_general_EISF(q, C3_C6_MA,18)


f, ax = plt.subplots(nrows =1, ncols = 2, figsize = (20,10), sharey = True)
f.subplots_adjust(wspace = 0.3)

ax[0].plot(q,FA_C6_C_center,c='k',label = '$C_6$ Flip about C', linewidth =2)
ax[0].plot(q,FA_C6_N_center,c= 'b', label ='$C_6$ Flip about N-N',linewidth =2)
ax[0].plot(q,FA_C6_centermass_center,c = 'r', label = '$C_6$ Flip about center of mass',linewidth =2)
ax[0].plot(q,FA_C6_twirl, c = 'c', label = '$C_6$ Rotation around carbon',linewidth =2)


ax[0].set_xlabel('Q $(A^{-1})$', fontsize = 16)
ax[0].set_ylabel('A$_Q$', fontsize = 16)

ax[1].plot(q,MA_C3_EISF,c = 'k',label = '$C_3$',linewidth =2)
ax[1].plot(q,MA_C6_EISF,c = 'b', label = '$C_6$',linewidth =2)
ax[1].plot(q,MA_C3_C6_EISF,c = 'r', label = '$C_{3x6}$',linewidth =2)



aj.figure_quality_axes(ax[0], r'Q $\bf(A^{-1})$',r'A$\bf_Q$', 'FA modes', legend = True)
aj.figure_quality_axes(ax[1], r'Q $\bf(A^{-1})$',r'A$\bf_Q$', 'MA modes', legend = True)

ax[1].set_yticklabels({})
ax[0].set_yticklabels({})

f.savefig('Figures/EISF modes.svg',format = 'svg',dpi=1200)
plt.show()


pickle.dump(FA_C6_C_center, open('FA_C6_C_Center_EISF.pickle','wb'))
pickle.dump(FA_C6_N_center, open('FA_C6_N_Center_EISF.pickle','wb'))
pickle.dump(FA_C6_centermass_center, open('FA_C6_Centermass_Center_EISF.pickle','wb'))
pickle.dump(FA_C6_twirl, open('FA_C6_twirl_EISF.pickle','wb'))
pickle.dump(MA_C3_C6_EISF, open('MA_total_EISF.pickle','wb'))
pickle.dump(MA_C3_EISF, open('MA_C3_EISF.pickle','wb'))
pickle.dump(MA_C6_EISF, open('MA_C6_EISF.pickle','wb'))
