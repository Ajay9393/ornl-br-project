# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 03:26:27 2019

@author: Andrew
"""

import numpy as np
import re
import pickle
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import matplotlib.lines as mlines
from matplotlib.pyplot import cm
from scipy.optimize import curve_fit
import itertools
from sklearn.metrics import r2_score
import pandas as pd
colors = cm.plasma(np.linspace(0.1,0.8,4))
import sys
sys.path.insert(0, '../../')
import DataAnalysis as aj



def fit_multiple_polys(list_of_functions):
    def sum_polys(x, *n1):
    ##n1 will be all fractions for MA rotations and all fraactions for FA rotations, in the order corresponding
    ##to the flattened array of polys passed (i.e all MA then all FA)
        
        MA_polyf = list_of_functions[0][:]
        FA_polyf = list_of_functions[1][:]
        
        num_MA = len(MA_polyf)
        num_FA = len(FA_polyf)
        temp = np.zeros(len(x))
        n1 = np.array(n1).ravel()
        for idx in range(0,num_MA):
            fcn = MA_polyf[idx]
            temp = temp + n1[idx]*(1.-fcn(x))
        for idx in range(0,num_FA):
            fcn = FA_polyf[idx]
            temp = temp + n1[idx + num_MA]*(1.-fcn(x))   

        return 1. - temp
    return sum_polys
    
        

EISF = pickle.load(open('EISF.pickle','rb'))

FA_C6_C_center_EISF = pickle.load(open('FA_C6_C_Center_EISF.pickle','rb'))
FA_C6_N_center_EISF = pickle.load(open('FA_C6_N_Center_EISF.pickle','rb'))
FA_C6_Centermass_center_EISF = pickle.load(open('FA_C6_Centermass_Center_EISF.pickle','rb'))
FA_C6_twirl_EISF = pickle.load(open('FA_C6_twirl_EISF.pickle','rb'))
#FA_C2_twirl_EISF = pickle.load(open('FA_C2_twirl_EISF.pickle','rb'))
MA_C3_C6_EISF = pickle.load(open('MA_total_EISF.pickle','rb'))
MA_C3_EISF = pickle.load(open('MA_C3_EISF.pickle','rb'))
MA_C6_EISF = pickle.load(open('MA_C6_EISF.pickle','rb'))

q = np.linspace(0,5,num=len(FA_C6_twirl_EISF))


FA_functions = {'FA_C6_C_Center':FA_C6_C_center_EISF,
                'FA_C6_N_Center':FA_C6_N_center_EISF,
                'FA_C6_Centermass_Center': FA_C6_Centermass_center_EISF,
                'FA_C6_Rotation': FA_C6_twirl_EISF}
                #'FA_C2_twirl': FA_C2_twirl_EISF}

MA_functions = {'MA_C3': MA_C3_EISF,
                'MA_C6': MA_C6_EISF,
                'MA_total': MA_C3_C6_EISF}


FA_poly = {}

MA_poly = {}



for idx, key in enumerate(FA_functions.keys()):
    FA_poly[key] = np.poly1d(np.polyfit(q, FA_functions[key],15))



for idx, key in enumerate(MA_functions.keys()):
    MA_poly[key] = np.poly1d(np.polyfit(q, MA_functions[key],15))




bulk  = [x for x in EISF.keys() if re.findall('Bulk',x) and re.findall('Narrow',x)]


dict_to_fit = {x:EISF[x] for x in bulk if re.findall('80',x) or re.findall('100',x)}

all_vals_nar = np.zeros([16,2])
all_errs_nar = np.zeros([16,2])
all_r_nar = np.zeros([16,1])
order = []

for i, key in enumerate(sorted(dict_to_fit.keys(), key = lambda x: ( int(re.findall(r'(\d+)\s?K',x)[0]),int(re.findall(r'(\d+)Br',x)[0])) )):
    order.append(key)
    MA_fcn = []
    FA_fcn = [FA_poly['FA_C6_N_Center']]
    q_val = EISF[key][0][:]
    val = EISF[key][1][:]
    bounds = (np.zeros(len(FA_fcn + MA_fcn)), np.concatenate(([0.15*6./(0.8*5. + 0.15*6.)]*len(MA_fcn),[0.8*5./(0.8*5. + 0.15*6.)]*len(FA_fcn))))

    p0 = np.concatenate(([0.1]*len(MA_fcn), [0.5]*len(FA_fcn)))
    pp1, pp2 = curve_fit(fit_multiple_polys([MA_fcn,FA_fcn ]),xdata = q_val,ydata = val, p0 =p0, bounds = bounds)
    y_pred = fit_multiple_polys([MA_fcn, FA_fcn])(q_val, *pp1)
    y_temp = fit_multiple_polys([MA_fcn, FA_fcn])(q, *pp1)
    r_squared = r2_score(val,y_pred)
    
    all_vals_nar[i, 1] = pp1
    all_errs_nar[i,1] = np.sqrt(np.diag(pp2))
    all_r_nar[i,0] =  r_squared
    
    
    
    
    

dict_to_fit = {x:EISF[x] for x in bulk if re.findall('120',x) or re.findall('160',x)}

for i, key in enumerate(sorted(dict_to_fit.keys(), key = lambda x: ( int(re.findall(r'(\d+)\s?K',x)[0]),int(re.findall(r'(\d+)Br',x)[0])) )):
    order.append(key)
    MA_fcn = []
    FA_fcn = [FA_poly['FA_C6_N_Center']]
    q_val = EISF[key][0][:]
    val = EISF[key][1][:]
    bounds = (np.zeros(len(FA_fcn + MA_fcn)), np.concatenate(([0.15*6./(0.8*5. + 0.15*6.)]*len(MA_fcn),[0.8*5./(0.8*5. + 0.15*6.)]*len(FA_fcn))))

    p0 = np.concatenate(([0.1]*len(MA_fcn), [0.5]*len(FA_fcn)))
    pp1, pp2 = curve_fit(fit_multiple_polys([MA_fcn,FA_fcn ]),xdata = q_val,ydata = val, p0 =p0, bounds = bounds)
    y_pred = fit_multiple_polys([MA_fcn, FA_fcn])(q_val, *pp1)
    y_temp = fit_multiple_polys([MA_fcn, FA_fcn])(q, *pp1)
    r_squared = r2_score(val,y_pred)
    
    all_vals_nar[i+8, 1] = pp1
    all_r_nar[i+8,0] =  r_squared
    all_errs_nar[i+8,1] = np.sqrt(np.diag(pp2))
    
    
f, ax = plt.subplots(nrows = 1, ncols = 2, figsize = [16,8])
f.tight_layout()
f.subplots_adjust(wspace = 0.2, hspace = 0.1, top = 0.95, bottom = 0.05, left = 0.05, right = 0.95)

T = [80, 100, 120, 160]
labels = ['0% Br', '10% Br', '15% Br', '20% Br']
for idx in range(0,4):
    ax[0].plot(T, 1.-all_vals_nar[idx::4][:,0]/0.18367347,'o:', color = colors[idx], label = labels[idx], markersize = 10, linewidth = 3)
    ax[1].plot(T, 1.-all_vals_nar[idx::4][:,1]/0.81632653, 'o:',color = colors[idx], label = labels[idx], markersize = 10, linewidth = 3)
    
ax[0].set_ylim([0,1.05])
ax[1].set_ylim([0,1.05])
aj.figure_quality_axes(ax[0],'Temperature (K)','Fraction of inactive MA','', legend = True)
aj.figure_quality_axes(ax[1],'Temperature (K)','Fraction of inactive FA ','', legend = True)


np.savetxt('All_narrow_fits.csv', np.concatenate((all_vals_nar, all_errs_nar, all_r_nar), axis  = 1), delimiter  = ',')   
f.savefig('Narrow Fractions.svg',format = 'svg', dpi = 1200)

############################################################################################
#Broad Lorentzian

bulk  = [x for x in EISF.keys() if re.findall('Bulk',x) and re.findall('Broad',x)]

dict_to_fit = {x:EISF[x] for x in bulk if re.findall('80',x) or re.findall('100',x)}

all_vals_bro = np.zeros([16,2])
all_errs_bro = np.zeros([16,2])
all_r_bro = np.zeros([16,1])
order = []

for i, key in enumerate(sorted(dict_to_fit.keys(), key = lambda x: ( int(re.findall(r'(\d+)\s?K',x)[0]),int(re.findall(r'(\d+)Br',x)[0])) )):
    order.append(key)
    MA_fcn = [MA_poly['MA_C3']]
    FA_fcn = [FA_poly['FA_C6_Rotation']]
    q_val = EISF[key][0][:]
    val = EISF[key][1][:]
    bounds = ([0,0], np.concatenate(([0.15*6./(0.8*5. + 0.15*6.)],[0.8*5./(0.8*5. + 0.15*6.)])))
    p0 = np.array([0.1, 0.5])
    pp1, pp2 = curve_fit(fit_multiple_polys([MA_fcn,FA_fcn ]),xdata = q_val,ydata = val, p0 =p0, bounds = bounds)
    y_pred = fit_multiple_polys([MA_fcn, FA_fcn])(q_val, *pp1)
    y_temp = fit_multiple_polys([MA_fcn, FA_fcn])(q, *pp1)
    r_squared = r2_score(val,y_pred)
    
    all_vals_bro[i, :] = pp1
    all_errs_bro[i,:] = np.sqrt(np.diag(pp2))
    all_r_bro[i,0] =  r_squared
    

dict_to_fit = {x:EISF[x] for x in bulk if re.findall('120',x) or re.findall('160',x)}

for i, key in enumerate(sorted(dict_to_fit.keys(), key = lambda x: ( int(re.findall(r'(\d+)\s?K',x)[0]),int(re.findall(r'(\d+)Br',x)[0])) )):
    order.append(key)
    MA_fcn = [MA_poly['MA_C3']]
    print 'using MA_C3!'
    FA_fcn = [FA_poly['FA_C6_Rotation']]
    q_val = EISF[key][0][:]
    val = EISF[key][1][:]
    bounds = ([0,0], np.concatenate(([0.15*6./(0.8*5. + 0.15*6.)],[0.8*5./(0.8*5. + 0.15*6.)])))
    p0 = np.array([0.1, 0.5])
    pp1, pp2 = curve_fit(fit_multiple_polys([MA_fcn,FA_fcn ]),xdata = q_val,ydata = val, p0 =p0, bounds = bounds)
    y_pred = fit_multiple_polys([MA_fcn, FA_fcn])(q_val, *pp1)
    y_temp = fit_multiple_polys([MA_fcn, FA_fcn])(q, *pp1)
    r_squared = r2_score(val,y_pred)
    
    all_vals_bro[i+8, :] = pp1
    all_r_bro[i+8,0] =  r_squared
    all_errs_bro[i+8,:] = np.sqrt(np.diag(pp2))
    
np.savetxt('All_broad_fits.csv', np.concatenate((all_vals_bro, all_errs_bro, all_r_bro), axis  = 1), delimiter  = ',')   
f2, ax2 = plt.subplots(nrows = 1, ncols = 2, figsize = [16,8])
f2.tight_layout()
f2.subplots_adjust(wspace = 0.2, hspace = 0.1, top = 0.95, bottom = 0.05, left = 0.05, right = 0.95)


T = [80, 100, 120, 160]
labels = ['0% Br', '10% Br', '15% Br', '20% Br']
for idx in range(0,4):
    ax2[0].plot(T, 1.-all_vals_bro[idx::4][:,0]/0.18367347,'o:', color = colors[idx], label = labels[idx], markersize = 10, linewidth = 3)
    ax2[1].plot(T, 1.-all_vals_bro[idx::4][:,1]/0.81632653, 'o:',color = colors[idx], label = labels[idx], markersize = 10, linewidth = 3)
    
ax2[0].set_ylim([-0.05,1.05])
ax2[1].set_ylim([-0.05,1.05])
aj.figure_quality_axes(ax2[0],'Temperature (K)','Fraction of inactive MA','', legend = True)
aj.figure_quality_axes(ax2[1],'Temperature (K)','Fraction of inactive FA','', legend = True)


f2.savefig('Broad Fractions.svg',format = 'svg', dpi = 1200)
    
    
    


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    





