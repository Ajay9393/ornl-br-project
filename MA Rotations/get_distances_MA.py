# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 14:11:04 2019

@author: ak4jo
"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import proj3d
import re
from scipy.optimize import root
import pickle
import sys
sys.path.insert(0, '../../../')
import DataAnalysis as aj


def get_center_mass(center, positions, masses):
    return np.sum(masses*(positions - center), axis = 0)

def apply_rotation(R, data_points):
    ##takes data_points which are list of positions i.e [[x,y,z],[x,y,x]]
    return np.transpose(np.dot(R,np.transpose(data_points)))

def generate_rotation_about_axis(V, theta):
    ##Takes a unit vector and an angle and returns the matrix rotating points about an axis described by that unit vector
    M = np.array(\
                [[V[0]**2.0 + (V[1]**2.0+V[2]**2.0)*np.cos(theta),\
                  V[0]*V[1]*(1-np.cos(theta)) - V[2]*np.sin(theta), \
                  V[0]*V[2]*(1-np.cos(theta))+V[1]*np.sin(theta)],\
                [V[0]*V[1]*(1- np.cos(theta))+V[2]*np.sin(theta),\
                 V[1]**2.0 + (V[0]**2.0+V[2]**2.0)*np.cos(theta),\
                 V[1]*V[2]*(1-np.cos(theta)) - V[0]*np.sin(theta)],
                 [V[0]*V[2]*(1-np.cos(theta)) - V[1]*np.sin(theta),\
                  V[1]*V[2]*(1-np.cos(theta)) + V[0]*np.sin(theta),\
                  V[2]**2.0+(V[0]**2.0+V[1]**2.0)*np.cos(theta)]])
    return M


def generate_rotation_vector(a,b):
    ##generate matrix to rotate from a to b
    v = np.cross(a, b)
    c = np.dot(a, b)
    G = np.array([[c,-np.linalg.norm(v),0],[np.linalg.norm(v),c,0],[0,0,1]])
    x_1 = np.dot(a,b)*a/(np.linalg.norm(np.dot(a,b)*a))
    x_2 = (b - np.dot(a,b)*a)/(np.linalg.norm(b-np.dot(a,b)*a))
    x_3 = np.cross(b,a)
    F = np.linalg.inv(np.transpose(np.array([x_1,x_2,x_3])))
    invF = np.linalg.inv(F)
    R = np.dot(invF,np.dot(G,F))
    return R

def annotate2D(ax,labels, datapoints):
    ##Labels the points of a 2D plot
    for label, x,y in zip(labels, datapoints[:,0],datapoints[:,1]):
        ax.annotate(
        label,
        fontsize = 18,
        xy=(x, y), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))

def annotate3D(ax,labels,datapoints):
    for idx,label in enumerate(labels):
        x2,y2,_ = proj3d.proj_transform(datapoints[idx,0],datapoints[idx,1],datapoints[idx,2],ax.get_proj())
        ax.annotate(
            label,
            xy=(x2, y2), xytext=(-20, 20),
            textcoords='offset points', ha='right', va='bottom',
            bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
            arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))




#from 295 K data
N = np.array([0.06960,0.43040,0.28580])*np.array([8.87960,8.87960,12.62660])
H1 = np.array([0.16490,0.49330,0.30000])*np.array([8.87960,8.87960,12.62660])
H2 = np.array([0.09520,0.34390,0.23450])*np.array([8.87960,8.87960,12.62660])
H3 = np.array([0.03040,0.38640,0.35600])*np.array([8.87960,8.87960,12.62660])
C = np.array([-0.04660,0.52700,0.23850])*np.array([8.87960,8.87960,12.62660])
H4 = np.array([-0.07570,0.61330,0.29250])*np.array([8.87960,8.87960,12.62660])
H5 = np.array([-0.14330,0.46100,0.22130])*np.array([8.87960,8.87960,12.62660])
H6 = np.array([-0.00490,0.57500,0.16760])*np.array([8.87960,8.87960,12.62660])
data_points = np.array([N,H1,H2,H3,C,H4,H5,H6])
labels = ['N','H1','H2','H3','C','H4','H5','H6']


#with open('MA.sdf','rb') as f:
#    Data = f.read().splitlines()
#
#num_elements = 8
#
#
#flag = re.compile(r'\s+')
#x_pos = [float(flag.split(y)[1]) for y in Data[4:4+num_elements]]
#y_pos = [float(flag.split(y)[2]) for y in Data[4:4+num_elements]]
#z_pos = [float(flag.split(y)[3]) for y in Data[4:4+num_elements]]
#
#
#labels = [flag.split(y)[4] for y in Data[4:4+num_elements]]
#
#
#data_points = np.array([[x_pos[idx], y_pos[idx],z_pos[idx]] for idx, ele in enumerate(labels)])

np.save('MA_positions.npy',data_points)
pickle.dump(labels, open('labels.pickle','wb'))

print data_points



##rotating so that the C-N axis points in the z direction
data_points = data_points - data_points[labels.index('N')]
unit_C_N = -1.0/np.linalg.norm(data_points[labels.index('C')]-data_points[labels.index('N')])*(data_points[labels.index('C')]-data_points[labels.index('N')])

R_temp = generate_rotation_vector(unit_C_N, 1./np.sqrt(2)*np.array([1,0,1], dtype = float))

data_points = apply_rotation(R_temp, data_points)
data_points = data_points - data_points[labels.index('C')]

unit_C_N = -1.0/np.linalg.norm(data_points[labels.index('C')]-data_points[labels.index('N')])*(data_points[labels.index('C')]-data_points[labels.index('N')])
R = generate_rotation_vector(unit_C_N, np.array([0,0,1], dtype = float))
data_points = apply_rotation(R, data_points)
np.save('rotation1.npy',data_points)


data_points=data_points - data_points[2]
unit_H1_H2 = 1.0/np.linalg.norm(data_points[2]-data_points[3])*(data_points[2]-data_points[3])

#R = generate_rotation_vector(unit_H1_H2, np.array([0,1,0], dtype = float))
#
#data_points = apply_rotation(R, data_points)
print "\n"

print"After Rotation" + "\n"
print data_points
print "\n"

#print "The distance between C and N is {0:.2f} A".format(np.linalg.norm(data_points[labels.index('C')] - data_points[labels.index('N')]))
#print "The distance between C and H4 is {0:.2f} A".format(np.linalg.norm(data_points[labels.index('C')] - data_points[labels.index('H4')]))
#print "The distance between C and H5 is {0:.2f} A".format(np.linalg.norm(data_points[labels.index('C')] - data_points[labels.index('H5')]))
#print "The distance between C and H6 is {0:.2f} A".format(np.linalg.norm(data_points[labels.index('C')] - data_points[labels.index('H6')]))
#print "The distance between N and H1 is {0:.2f} A".format(np.linalg.norm(data_points[labels.index('N')] - data_points[labels.index('H1')]))
#print "The distance between N and H2 is {0:.2f} A".format(np.linalg.norm(data_points[labels.index('N')] - data_points[labels.index('H2')]))
#print "The distance between N and H3 is {0:.2f} A".format(np.linalg.norm(data_points[labels.index('N')] - data_points[labels.index('H3')]))

f1 = plt.figure(figsize = [10,10])

ax1 = f1.add_subplot(111)

for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax1.scatter(data_points[idx][0],data_points[idx][2], c = colour)

annotate2D(ax1, labels, np.transpose(np.array([data_points[:,0],data_points[:,2]])))

masses = np.array([14,1,1,1,12,1,1,1]).reshape([8,1])

##Get center of mass
center = root(get_center_mass, x0 = [0,0,0], args = (data_points,masses))
center_coords = center.x

##For not rotation about center of mass
##data_points = data_points - (data_points[labels.index('C')] + data_points[labels.index('N')])/2.0

##For rotation about center of mass
data_points = data_points - center_coords
center_coords = center_coords - center_coords

f2 = plt.figure(figsize = [10,10])

ax2 = f2.add_subplot(111)
ax2.scatter(0, 0,c = 'g')
for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax2.scatter(data_points[idx][0],data_points[idx][2], c = colour)

annotate2D(ax2,labels,np.transpose(np.array([data_points[:,0],data_points[:,2]])))

list_of_figures = [f1,f2]
list_of_axes = [ax1,ax2]
##Get distances for all 6 hydrogens for all 12 possible rotations

list_of_distances = np.zeros([3,6,6])

rotations_C6 = np.array([0, 2*np.pi/6, 2*2*np.pi/6, 3*2*np.pi/6, 4*2*np.pi/6, 5*2*np.pi/6], dtype  = np.longdouble)
rotations_C3 = np.array([0, 2*np.pi/3, 4*np.pi/3], dtype = np.longdouble)
rot_names_C6 = [r'\bf{E}',r'\bf{C_6}',r'\bf{C_3}',r'\bf{C_2}',r'\bf{C^{2}_3}',r'\bf{C^P5}_6}']
rot_names_C3 = ['E','C_3','C^{2}_3']

list_of_postitions = np.zeros([3,6,8,3])

f_final, ax = plt.subplots(nrows = 6, ncols = 3,figsize = [18,30])
f_final.tight_layout()
f_final.subplots_adjust(wspace = 0.4, hspace = 0.4, top = 0.95, bottom = 0.05)



for idx1, angle1 in enumerate(rotations_C3):

    for idx2, angle2 in enumerate(rotations_C6):

        R1 = generate_rotation_about_axis([0,0,1], angle1)
        R2 = generate_rotation_about_axis([0,1,0], angle2)

        new_data_points = apply_rotation(np.dot(R2,R1), data_points)
        print new_data_points
        list_of_postitions[idx1,idx2,:] = new_data_points
        print '\n'
        list_of_distances[idx1,idx2,:] = [x for i,x in enumerate(np.linalg.norm(new_data_points - data_points, axis = 1)) if re.findall('H',labels[i])]
        for idx, label in enumerate(labels):
            if re.findall('C',label):
                colour = 'k'
            elif re.findall('N',label):
                colour = 'b'
            elif re.findall('H',label):
                colour = 'r'
            else:
                colour = 'g'
            ax[idx2,idx1].scatter(new_data_points[idx][0],new_data_points[idx][2], c = colour)

        ax[idx2,idx1].annotate('Rotation $' + rot_names_C3[idx1] +'$ + $' + rot_names_C6[idx2] +'$', xy = (0,0), fontsize = 20, weight = 'bold', ha = 'center', va = 'center')
        annotate2D(ax[idx2, idx1],labels,np.transpose(np.array([new_data_points[:,0],new_data_points[:,2]])))
        aj.figure_quality_axes(ax[idx2,idx1], 'Position (A)', 'Position (A)','',legend = False )




C3_distances = list_of_distances[:,0,:]
C6_distances = list_of_distances[0,:,:]
C3_C6_distances = list_of_distances.reshape([18,6], order = 'C')

list_of_postitions = list_of_postitions.reshape(18,8,3)
np.save('C3_distances.npy',C3_distances)
np.save('C6_distances.npy',C6_distances)
np.save('C3_C6_distances.npy',C3_C6_distances)
np.savetxt('C3_C6_distances.csv',C3_C6_distances, delimiter = ',')

pickle.dump(labels,open('labels.pickle','wb'))


f_final.savefig('MA_rotations.svg', format = 'svg', dpi = 1200)




plt.show()
