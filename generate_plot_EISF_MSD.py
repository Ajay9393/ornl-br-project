# -*- coding: utf-8 -*-
"""
Created on Fri Nov 09 10:38:46 2018

@author: ak4jo
"""

##Make plots for neutron Basis measurements

import os
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
from scipy.special import jv


working_directory = '../20181011 Trip/'


folders = [x for x in os.listdir(working_directory) if re.findall('Sample',x)]
MSD = {}
EISF = {}
try:
    EISF = pickle.load(open(working_directory  + 'EISF.pickle','rb'))
    MSD = pickle.load(open(working_directory  + 'MSD.pickle','rb'))
    print 'Found them!'
except (EOFError,IOError) as e:
    for folder in folders:
        for sample in os.listdir(folder):
            with open(working_directory+r'/'+folder +r'/'+sample + r'/msd.txt') as f:
                Data = f.read().splitlines()
            start_idx = Data.index('2') + 1
            Temp = [float(x.split(',')[0]) for x in Data[start_idx:]]
            val = [float(x.split(',')[1]) for x in Data[start_idx:]]
            err = [float(x.split(',')[2]) for x in Data[start_idx:]]
            
            MSD[folder + r'/'+sample] = np.array([Temp,val,err])
            
            for temp in [x for x in os.listdir(working_directory + r'/' + folder + r'/' +sample) if not re.findall(r'\.', x)]:
                for eisf in [x for x in os.listdir(working_directory + r'/' + folder + r'/' +sample + r'/' + temp) if re.findall('EISF',x)]:
                    with open(working_directory + r'/' + folder + r'/' +sample + r'/' + temp+r'/'+eisf) as f:
                        Data_2 = f.read().splitlines()
                    q = [float(x.split('    ')[1]) for x in Data_2[1:]]
                    coeff = [float(x.split('    ')[2]) for x in Data_2[1:]]
                    err = [float(x.split('    ')[3]) for x in Data_2[1:]]
                    EISF[folder+'/'+sample+'/'+temp+'/'+eisf] = np.array([q,coeff,err])
    
    pickle.dump(EISF, open(working_directory + 'EISF.pickle','wb'))   
    print 'Saving them'
    pickle.dump(MSD, open(working_directory + 'MSD.pickle','wb'))             
f, ax = plt.subplots(nrows = 2, ncols = 2, figsize = (8,8))
f.subplots_adjust(wspace = 0.4, hspace = 0.4)
temp = [x for x in MSD.keys() if re.findall('Bulk',x)]
temp.sort()
for idx,key in enumerate(temp):
    
    ax[idx/2, idx%2].plot(MSD[key][0,:], MSD[key][1,:])
    ax[idx/2, idx%2].set_title(key)
    ax[idx/2, idx%2].set_xlabel('Temperature (K)', fontsize = 16)
    ax[idx/2, idx%2].set_ylabel(r'<$d^2$>', fontsize = 16)
    
f2, ax2 = plt.subplots(nrows = 1, ncols = 3, figsize = (10,4))
f2.subplots_adjust(wspace = 0.4, hspace = 0.4)
temp = [x for x in MSD.keys() if re.findall('PEA',x)]
temp.sort()
for idx,key in enumerate(temp):
    
    ax2[idx].plot(MSD[key][0,:], MSD[key][1,:])
    ax2[idx].set_title(key)
    ax2[idx].set_xlabel('Temperature (K)', fontsize = 16)
    ax2[idx].set_ylabel(r'<$d^2$>', fontsize = 16)
    

f3, ax3 = plt.subplots(nrows = 1, ncols = 3, figsize = (10,4))
f3.subplots_adjust(wspace = 0.4, hspace = 0.4)
temp = [x for x in MSD.keys() if re.findall('BTA',x)]
temp.sort()

for idx,key in enumerate(temp):
    
    ax3[idx].plot(MSD[key][0,:], MSD[key][1,:])
    ax3[idx].set_title(key)
    ax3[idx].set_xlabel('Temperature (K)')
    ax3[idx].set_ylabel(r'<$d^2$>')
    


temp = [x for x in EISF.keys() if re.findall('Bulk',x)]
temp.sort()
f4, ax4 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f4.subplots_adjust(wspace = 0.4, hspace = 0.4)
for idx, key in enumerate(temp[0:8]):
    ax4[idx/2/2,(idx/2)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = os.path.basename(key))
    ax4[idx/2/2,(idx/2)%2].set_title(re.findall(r'\d+K',key)[0])
    ax4[idx/2/2,(idx/2)%2].set_xlabel('q')
    ax4[idx/2/2,(idx/2)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax4[idx/2/2, (idx/2)%2].legend()
    
f4.suptitle('0% Br')   
 
f5, ax5 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f5.subplots_adjust(wspace = 0.4, hspace = 0.4)
for idx, key in enumerate(temp[8:16]):
    ax5[idx/2/2,(idx/2)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = os.path.basename(key))
    ax5[idx/2/2,(idx/2)%2].set_title(re.findall(r'\d+\s+?K',key)[0])
    ax5[idx/2/2,(idx/2)%2].set_xlabel('q')
    ax5[idx/2/2,(idx/2)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax5[idx/2/2, (idx/2)%2].legend()
    
f5.suptitle('10% Br')

f6, ax6 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f6.subplots_adjust(wspace = 0.4, hspace = 0.4)

for idx, key in enumerate(temp[16:24]):
    ax6[idx/2/2,(idx/2)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = os.path.basename(key))
    ax6[idx/2/2,(idx/2)%2].set_title(re.findall(r'\d+\s?K',key)[0])
    ax6[idx/2/2,(idx/2)%2].set_xlabel('q')
    ax6[idx/2/2,(idx/2)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax6[idx/2/2, (idx/2)%2].legend()
    
f6.suptitle('15% Br')
f7, ax7 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f7.subplots_adjust(wspace = 0.4, hspace = 0.4)
for idx, key in enumerate(temp[8:16]):
    ax7[idx/2/2,(idx/2)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = os.path.basename(key))
    ax7[idx/2/2,(idx/2)%2].set_title(re.findall(r'\d+\s?K',key)[0])
    ax7[idx/2/2,(idx/2)%2].set_xlabel('q')
    ax7[idx/2/2,(idx/2)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax7[idx/2/2, (idx/2)%2].legend()
    
f7.suptitle('20% Br')

temp = sorted(temp,key = lambda x: int(re.findall(r'(\d+)\s?K',x)[0]))

f8, ax8 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f8.subplots_adjust(wspace = 0.4, hspace = 0.4)
for idx, key in enumerate([ x for x in temp if re.findall('Broad',x)]):
    ax8[idx/4/2,(idx/4)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = re.findall(r'\d+Br',os.path.dirname(key))[0])
    ax8[idx/4/2,(idx/4)%2].set_title(re.findall(r'\d+\s?K',key)[0])
    ax8[idx/4/2,(idx/4)%2].set_xlabel('q')
    ax8[idx/4/2,(idx/4)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax8[idx/4/2, (idx/4)%2].legend()
    
f8.suptitle('Broad LF of Bulk')


f9, ax9 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f9.subplots_adjust(wspace = 0.4, hspace = 0.4)
for idx, key in enumerate([ x for x in temp if re.findall('Narrow',x)]):
    ax9[idx/4/2,(idx/4)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = re.findall(r'\d+Br',os.path.dirname(key))[0])
    ax9[idx/4/2,(idx/4)%2].set_title(re.findall(r'\d+\s?K',key)[0])
    ax9[idx/4/2,(idx/4)%2].set_xlabel('q')
    ax9[idx/4/2,(idx/4)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax9[idx/4/2, (idx/4)%2].legend()
    
f9.suptitle('Narrow LF of Bulk')

temp = [x for x in EISF.keys() if re.findall('BTA',x)]
temp = sorted(temp,key = lambda x: int(re.findall(r'(\d+)\s?K',x)[0]))
    
f10, ax10 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f10.subplots_adjust(wspace = 0.4, hspace = 0.4)
for idx,key in enumerate([x for x in temp if re.findall('Narrow',x)]):
    ax10[idx/2/2, (idx/2)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = re.findall(r'n=\d',os.path.dirname(key))[0])
    ax10[idx/2/2, (idx/2)%2].set_title(re.findall(r'\d+\s?K', key)[0])
    ax10[idx/2/2,(idx/2)%2].set_xlabel('q')
    ax10[idx/2/2,(idx/2)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax10[idx/2/2, (idx/2)%2].legend()
f10.suptitle('Narrow LF of BTA')
   
f11, ax11 = plt.subplots(nrows = 2, ncols = 2, figsize = (10,10))
f11.subplots_adjust(wspace = 0.4, hspace = 0.4)
for idx,key in enumerate([x for x in temp if re.findall('Broad',x)]):
    ax11[idx/2/2, (idx/2)%2].scatter(EISF[key][0,:], EISF[key][1,:], label = re.findall(r'n=\d',os.path.dirname(key))[0])
    ax11[idx/2/2, (idx/2)%2].set_title(re.findall(r'\d+\s?K', key)[0])
    ax11[idx/2/2,(idx/2)%2].set_xlabel('q')
    ax11[idx/2/2,(idx/2)%2].set_ylabel('Elastic-Incoherent Structure Coefficient')
    ax11[idx/2/2, (idx/2)%2].legend()
f11.suptitle('Broad LF of BTA')  





    

                