# -*- coding: utf-8 -*-
"""
Created on Mon Jan 07 14:03:02 2019

@author: ak4jo
"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import proj3d
import re
from scipy.optimize import root
import pickle
import sys
sys.path.insert(0, '../../../')
import DataAnalysis as aj


def get_center_mass(center, positions, masses):
    return np.sum(masses*(positions - center), axis = 0)

def apply_rotation(R, data_points):
    ##takes data_points which are list of positions i.e [[x,y,z],[x,y,x]]
    return np.transpose(np.dot(R,np.transpose(data_points)))

def generate_rotation_about_axis(V, theta):
    ##Takes a unit vector and an angle and returns the matrix rotating points about an axis described by that unit vector
    M = np.array(\
                [[V[0]**2.0 + (V[1]**2.0+V[2]**2.0)*np.cos(theta),\
                  V[0]*V[1]*(1-np.cos(theta)) - V[2]*np.sin(theta), \
                  V[0]*V[2]*(1-np.cos(theta))+V[1]*np.sin(theta)],\
                [V[0]*V[1]*(1- np.cos(theta))+V[2]*np.sin(theta),\
                 V[1]**2.0 + (V[0]**2.0+V[2]**2.0)*np.cos(theta),\
                 V[1]*V[2]*(1-np.cos(theta)) - V[0]*np.sin(theta)],
                 [V[0]*V[2]*(1-np.cos(theta)) - V[1]*np.sin(theta),\
                  V[1]*V[2]*(1-np.cos(theta)) + V[0]*np.sin(theta),\
                  V[2]**2.0+(V[0]**2.0+V[1]**2.0)*np.cos(theta)]])
    return M


def generate_rotation_vector(a,b):
    ##generate matrix to rotate from a to b
    v = np.cross(a, b)
    c = np.dot(a, b)
    G = np.array([[c,-np.linalg.norm(v),0],[np.linalg.norm(v),c,0],[0,0,1]])
    x_1 = np.dot(a,b)*a/(np.linalg.norm(np.dot(a,b)*a))
    x_2 = (b - np.dot(a,b)*a)/(np.linalg.norm(b-np.dot(a,b)*a))
    x_3 = np.cross(b,a)
    F = np.linalg.inv(np.transpose(np.array([x_1,x_2,x_3])))
    invF = np.linalg.inv(F)
    R = np.dot(invF,np.dot(G,F))
    return R

def annotate2D(ax,labels, datapoints):
    ##Labels the points of a 2D plot
    for label, x,y in zip(labels, datapoints[:,0],datapoints[:,1]):
        ax.annotate(
        label,
        xy=(x, y), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))

def annotate3D(ax,labels,datapoints):
    for idx,label in enumerate(labels):
        x2,y2,_ = proj3d.proj_transform(datapoints[idx,0],datapoints[idx,1],datapoints[idx,2],ax.get_proj())
        ax.annotate(
            label,
            xy=(x2, y2), xytext=(-20, 20),
            textcoords='offset points', ha='right', va='bottom',
            bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
            arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))

#C = np.array([-0.079, -0.014,-0.022])*np.array([5.9677,5.9676,5.9676])
#H1 = np.array([-0.2069,-0.1026,-0.0096])*np.array([5.9677,5.9676,5.9676])
#N1 = np.array([-0.038,0.122,0.141])*np.array([5.9677,5.9676,5.9676])
#H2 = np.array([0.0782,0.2069,0.13620])*np.array([5.9677,5.9676,5.9676])
#H3 = np.array([-0.1267,0.12760,0.25440])*np.array([5.9677,5.9676,5.9676])
#N2 = np.array([0.04,-0.038,-0.201])*np.array([5.9677,5.9676,5.9676])
#H4 = np.array([0.1596,0.0408,-0.2186])*np.array([5.9677,5.9676,5.9676])
#H5 = np.array([0.0008,-0.13270,-0.3016])*np.array([5.9677,5.9676,5.9676])
        
C = np.array([0.42960,0.34780  ,  0.66060])*np.array([4.82110 ,13.77600 , 7.01130])
H1 = np.array([0.42330 ,   0.29940 ,   0.56370])*np.array([4.82110,13.77600,7.01130])
N1 = np.array([0.61200  ,  0.41710 ,   0.65690])*np.array([4.82110,13.77600,7.01130])
H2 = np.array([0.72500  ,  0.41740  ,  0.56500])*np.array([4.82110,13.77600,7.01130])
H3 = np.array([0.62510  ,  0.46480   , 0.74540])*np.array([4.82110,13.77600,7.01130])
N2 = np.array([0.25400  ,  0.33890  ,  0.78600])*np.array([4.82110,13.77600,7.01130])
H4 = np.array([0.13520 ,   0.28820  ,  0.77760])*np.array([4.82110,13.77600,7.01130])
H5 = np.array([0.25150 ,   0.38320  ,  0.88010])*np.array([4.82110,13.77600,7.01130])      
        


data_points = np.array([C,H1,N1,H2,H3,N2,H4,H5])
labels = ['C','H1','N1','H2','H3','N2','H4','H5']

print "\n"

print "The distance between C and N1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[2]))
print "The distance between C and N2 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[5]))
print "The distance between N1 and N2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[5]))
print "The distance between C and H1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[1]))
print "The distance between N1 and H2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[3]))
print "The distance between N1 and H3 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[4]))
print "The distance between N2 and H4 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[6]))
print "The distance between N2 and H5 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[7]))

f1 = plt.figure()

ax1 = f1.add_subplot(111)

for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax1.scatter(data_points[idx][0],data_points[idx][2], c = colour)

annotate2D(ax1, labels, np.transpose(np.array([data_points[:,0],data_points[:,2]])))

##rotating so that the N-N axis points in the z direction
data_points = data_points - data_points[2]
unit_N_N = 1.0/np.linalg.norm(data_points[2]-data_points[5])*(data_points[2]-data_points[5])

R = generate_rotation_vector(unit_N_N, np.array([0,0,-1], dtype = float))


data_points = apply_rotation(R, data_points)

print data_points

np.save('rotation1.npy',data_points)


f2 = plt.figure()

ax2 = f2.add_subplot(111)

for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax2.scatter(data_points[idx][0],data_points[idx][2], c = colour)

annotate2D(ax2, labels, np.transpose(np.array([data_points[:,0],data_points[:,2]])))

#Rotating so that the C-N plane is in the the z-y plane
data_points = data_points - data_points[0]

#unit_C_N = 1.0/np.linalg.norm(data_points[0] - (data_points[2]+data_points[5])/2.0)*(data_points[0] - (data_points[2]+data_points[5])/2.0)
unit_C_H = 1.0/np.linalg.norm(data_points[0] - data_points[1])*(data_points[0] - data_points[1])
R_3 = generate_rotation_vector(unit_C_H,np.array([1,0,0], dtype = float))
data_points = apply_rotation(R_3,data_points)

print "\n"

print"After Rotation 2" + "\n"
print data_points
np.save('rotation2.npy',data_points)
print "The C-N bond plane points in the direction {0}".format(1.0/np.linalg.norm(data_points[0] - data_points[1])*(data_points[0] - data_points[1]))


f3,ax3 = plt.subplots(nrows = 6, ncols = 4, figsize = [20,30])
f3.tight_layout()
f3.subplots_adjust(wspace = 0.4, hspace = 0.4, top = 0.95, bottom = 0.05)


masses = np.array([12,1,14,1,1,14,1,1]).reshape([8,1])
center = root(get_center_mass, x0 = [0,0,0], args = (data_points,masses))
center_coords = center.x

c_center = data_points
n_center = data_points - np.append(((data_points[2]+data_points[5])/2.)[0:2],0)
mass_center = data_points - center_coords

list_of_distances = np.zeros([4,6,5])

rot_names_C4 = [r'\bf{E}',r'\bf{C_6}',r'\bf{C_3}',r'\bf{C_2}',r'\bf{C^{2}_3}',r'\bf{C^{5}_6}']
rot_center_names = ['Flip about C','Flip about N-N','Flip about C.o.M.','Rotate about C']




list_of_new_positions = np.array([c_center, n_center, data_points - center_coords, c_center])

rot_vector = np.array([[0,0,1],[0,0,1],[0,0,1],[1,0,0]])
rot_angle = [0, 2*np.pi/6, 2*2*np.pi/6, 3*2*np.pi/6, 4*2*np.pi/6, 5*2*np.pi/6]


for i, start in enumerate(list_of_new_positions):
    
    for k, rot in enumerate(rot_angle):

        R = generate_rotation_about_axis(rot_vector[i], rot)
        print R
        print start
        temp_data = apply_rotation(R, start)
        list_of_distances[i,k,:] = [x for it,x in enumerate(np.linalg.norm(temp_data - start, axis = 1)) if re.findall('H',labels[it])]

        for idx, label in enumerate(labels):

            if re.findall('C',label):
                colour = 'k'
            elif re.findall('N',label):
                colour = 'b'
            elif re.findall('H',label):
                colour = 'r'
            else:
                colour = 'g'
            ax3[k,i].scatter(temp_data[idx][0],temp_data[idx][2], c = colour)

        #ax3[i,k].annotate('Rotation $' + rot_names_C4[k] +'$', xy = (0,0), fontsize = 20, weight = 'bold', ha = 'center', va = 'center')
        annotate2D(ax3[k,i], labels, np.transpose(np.array([temp_data[:,0],temp_data[:,2]])))

        aj.figure_quality_axes(ax3[k,i], 'Position (A)', 'Position (A)','',legend = False )
        if i==0:
            ax3[k,0].annotate('Rotation: $' + rot_names_C4[k] +'$',  xy=(0, 0.5), xytext=(-ax3[k,0].yaxis.labelpad - 15, 0),
                xycoords=ax3[k,0].yaxis.label, textcoords='offset points', ha = 'right', va = 'center',
                fontsize = 22, weight = 'bold')
        ax3[k,i].set_xlim([-1.5,1.5])
        ax3[k,i].set_ylim([-2,2])
    
    tit = rot_center_names[i]
    ax3[0,i].set_title(tit, fontsize = 22, y =1.1, weight = 'bold')
    
np.save('C-C-axis_C6.npy',list_of_distances[0,:,:])
np.save('N-N-axis_C6.npy',list_of_distances[1,:,:])
np.save('center_of_mass-axis_C6.npy',list_of_distances[2,:,:])
np.save('C6_twirl_C6.npy',list_of_distances[3,:,:])

np.savetxt('C-C-axis_C6.csv',list_of_distances[0,:,:], delimiter = ',')
np.savetxt('N-N-axis_C6.csv',list_of_distances[1,:,:], delimiter = ',')
np.savetxt('center_of_mass-axis_C6.csv',list_of_distances[2,:,:], delimiter = ',')
np.savetxt('C6_twirl_C6.csv',list_of_distances[3,:,:], delimiter = ',')

f3.savefig('FA Rotations.svg', format = 'svg', dpi = 1200)

### OLd, unused code--> the above for loop does all of this

###Distances for rotation about C
#C_pi_2_R= generate_rotation_about_axis([0,0,1], np.pi/2.0)
#C_pi_2_data_points = apply_rotation(C_pi_2_R,data_points)
#C_pi_2_d = np.linalg.norm(C_pi_2_data_points - data_points, axis = 1)
#C_pi_R= generate_rotation_about_axis([0,0,1], np.pi)
#C_pi_data_points = apply_rotation(C_pi_R,data_points)
#C_pi_d = np.linalg.norm(C_pi_data_points - data_points, axis = 1)
#C_3_pi_2_R= generate_rotation_about_axis([0,0,1], 3.0*np.pi/2.0)
#C_3_pi_2_data_points = apply_rotation(C_3_pi_2_R,data_points)
#C_3_pi_2_d = np.linalg.norm(C_3_pi_2_data_points - data_points, axis = 1)
#
#
#distances_C = np.array([C_pi_2_d, C_pi_d,C_3_pi_2_d])
#
###Distances for rotation about N-N
#data_points[:,0:2] = data_points[:,0:2] - data_points[2,0:2]
#print data_points
#print n_center
#
#f4 = plt.figure()
#
#ax4 = f4.add_subplot(111)
#
#for idx, label in enumerate(labels):
#    if re.findall('C',label):
#        colour = 'k'
#    elif re.findall('N',label):
#        colour = 'b'
#    elif re.findall('H',label):
#        colour = 'r'
#    else:
#        colour = 'g'
#    ax4.scatter(data_points[idx][0],data_points[idx][2], c = colour)
#
#annotate2D(ax4,labels,np.transpose(np.array([data_points[:,0],data_points[:,2]])))
#
#N_pi_2_R= generate_rotation_about_axis([0,0,1], np.pi/2.0)
#N_pi_2_data_points = apply_rotation(N_pi_2_R,data_points)
#N_pi_2_d = np.linalg.norm(N_pi_2_data_points - data_points, axis = 1)
#N_pi_R= generate_rotation_about_axis([0,0,1], np.pi)
#N_pi_data_points = apply_rotation(N_pi_R,data_points)
#N_pi_d = np.linalg.norm(N_pi_data_points - data_points, axis = 1)
#N_3_pi_2_R= generate_rotation_about_axis([0,0,1], 3.0*np.pi/2.0)
#N_3_pi_2_data_points = apply_rotation(N_3_pi_2_R,data_points)
#N_3_pi_2_d = np.linalg.norm(N_3_pi_2_data_points - data_points, axis = 1)
#
#distances_N = np.array([N_pi_2_d, N_pi_d,N_3_pi_2_d])
#
###Get center of mass
#
#masses = np.array([12,1,14,1,1,14,1,1]).reshape([8,1])
#center = root(get_center_mass, x0 = [0,0,0], args = (data_points,masses))
#center_coords = center.x
#ax4.scatter(center_coords[0], center_coords[2],c = 'g')
#
#data_points = data_points - center_coords
#
#f5 = plt.figure()
#
#ax5 = f5.add_subplot(111)
#
#for idx, label in enumerate(labels):
#    if re.findall('C',label):
#        colour = 'k'
#    elif re.findall('N',label):
#        colour = 'b'
#    elif re.findall('H',label):
#        colour = 'r'
#    else:
#        colour = 'g'
#    ax5.scatter(data_points[idx][0],data_points[idx][2], c = colour)
#
#annotate2D(ax5,labels,np.transpose(np.array([data_points[:,0],data_points[:,2]])))
#
#cen_pi_2_R= generate_rotation_about_axis([0,0,1], np.pi/2.0)
#cen_pi_2_data_points = apply_rotation(cen_pi_2_R,data_points)
#cen_pi_2_d = np.linalg.norm(cen_pi_2_data_points - data_points, axis = 1)
#cen_pi_R= generate_rotation_about_axis([0,0,1], np.pi)
#cen_pi_data_points = apply_rotation(cen_pi_R,data_points)
#cen_pi_d = np.linalg.norm(cen_pi_data_points - data_points, axis = 1)
#cen_3_pi_2_R= generate_rotation_about_axis([0,0,1], 3.0*np.pi/2.0)
#cen_3_pi_2_data_points = apply_rotation(cen_3_pi_2_R,data_points)
#cen_3_pi_2_d = np.linalg.norm(cen_3_pi_2_data_points - data_points, axis = 1)
#
#distances_cen = np.array([cen_pi_2_d, cen_pi_d,cen_3_pi_2_d])
#
#
#
###rotate around Carbon
#
#twirl_pi_2_R= generate_rotation_about_axis([1,0,0], np.pi/2.0)
#twirl_pi_2_data_points = apply_rotation(twirl_pi_2_R,data_points)
#twirl_pi_2_d = np.linalg.norm(twirl_pi_2_data_points - data_points, axis = 1)
#twirl_pi_R= generate_rotation_about_axis([1,0,0], np.pi)
#twirl_pi_data_points = apply_rotation(twirl_pi_R,data_points)
#twirl_pi_d = np.linalg.norm(twirl_pi_data_points - data_points, axis = 1)
#twirl_3_pi_2_R= generate_rotation_about_axis([1,0,0], 3.0*np.pi/2.0)
#twirl_3_pi_2_data_points = apply_rotation(twirl_3_pi_2_R,data_points)
#twirl_3_pi_2_d = np.linalg.norm(twirl_3_pi_2_data_points - data_points, axis = 1)
#
#distances_twirl = np.array([twirl_pi_2_d, twirl_pi_d,twirl_3_pi_2_d])
#
#dict_of_distances = {'About the Carbon':[C_pi_2_d,C_pi_d,C_3_pi_2_d], 'About the N-N': [N_pi_2_d, N_pi_d, N_3_pi_2_d],\
#                     'About the Center of Mass': [cen_pi_2_d, cen_pi_d, cen_3_pi_2_d], 'Twirl about C' : [twirl_pi_2_d, twirl_pi_d, twirl_3_pi_2_d]}
#
#
#for idx, key in enumerate(dict_of_distances.keys()):
#    print "\n"
#    for atom, distance in enumerate(dict_of_distances[key][0]):
#        print "For a pi/2 rotation {0} axis, the {1} atom travelled a distance of {2:.2f} A".format(\
#                                   key, labels[atom], distance)
#    print "\n"
#    for atom, distance in enumerate(dict_of_distances[key][1]):
#        print "For a pi rotation {0} axis, the {1} atom travelled a distance of {2:.2f} A".format(\
#                                   key, labels[atom], distance)
#    print "\n"
#    for atom, distance in enumerate(dict_of_distances[key][2]):
#        print "For a 3pi/2 rotation {0} axis, the {1} atom travelled a distance of {2:.2f} A".format(\
#                                   key, labels[atom], distance)
