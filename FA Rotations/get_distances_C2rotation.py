# -*- coding: utf-8 -*-
"""
Created on Wed Jan 09 19:29:34 2019

@author: ak4jo
"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import proj3d
import re
from scipy.optimize import root
import pickle

def get_center_mass(center, positions, masses):
    return np.sum(masses*(positions - center), axis = 0)
    
def apply_rotation(R, data_points):
    ##takes data_points which are list of positions i.e [[x,y,z],[x,y,x]]
    return np.transpose(np.dot(R,np.transpose(data_points)))
    
def generate_rotation_about_axis(V, theta):
    ##Takes a unit vector and an angle and returns the matrix rotating points about an axis described by that unit vector
    M = np.array(\
                [[V[0]**2.0 + (V[1]**2.0+V[2]**2.0)*np.cos(theta),\
                  V[0]*V[1]*(1-np.cos(theta)) - V[2]*np.sin(theta), \
                  V[0]*V[2]*(1-np.cos(theta))+V[1]*np.sin(theta)],\
                [V[0]*V[1]*(1- np.cos(theta))+V[2]*np.sin(theta),\
                 V[1]**2.0 + (V[0]**2.0+V[2]**2.0)*np.cos(theta),\
                 V[1]*V[2]*(1-np.cos(theta)) - V[0]*np.sin(theta)],
                 [V[0]*V[2]*(1-np.cos(theta)) - V[1]*np.sin(theta),\
                  V[1]*V[2]*(1-np.cos(theta)) + V[0]*np.sin(theta),\
                  V[2]**2.0+(V[0]**2.0+V[1]**2.0)*np.cos(theta)]])
    return M
                 

def generate_rotation_vector(a,b):
    ##generate matrix to rotate from a to b
    v = np.cross(a, b)
    c = np.dot(a, b)
    G = np.array([[c,-np.linalg.norm(v),0],[np.linalg.norm(v),c,0],[0,0,1]])
    x_1 = np.dot(a,b)*a/(np.linalg.norm(np.dot(a,b)*a))
    x_2 = (b - np.dot(a,b)*a)/(np.linalg.norm(b-np.dot(a,b)*a))
    x_3 = np.cross(b,a)
    F = np.linalg.inv(np.transpose(np.array([x_1,x_2,x_3])))
    invF = np.linalg.inv(F)
    R = np.dot(invF,np.dot(G,F))
    return R

def annotate2D(ax,labels, datapoints):
    ##Labels the points of a 2D plot
    for label, x,y in zip(labels, datapoints[:,0],datapoints[:,1]):
        ax.annotate(
        label,
        xy=(x, y), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
    
def annotate3D(ax,labels,datapoints):
    for idx,label in enumerate(labels):
        x2,y2,_ = proj3d.proj_transform(datapoints[idx,0],datapoints[idx,1],datapoints[idx,2],ax.get_proj())    
        ax.annotate(
            label,
            xy=(x2, y2), xytext=(-20, 20),
            textcoords='offset points', ha='right', va='bottom',
            bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
            arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))

C = np.array([-0.079, -0.014,-0.022])*np.array([5.9677,5.9676,5.9676])
H1 = np.array([-0.2069,-0.1026,-0.0096])*np.array([5.9677,5.9676,5.9676])
N1 = np.array([-0.038,0.122,0.141])*np.array([5.9677,5.9676,5.9676])
H2 = np.array([0.0782,0.2069,0.13620])*np.array([5.9677,5.9676,5.9676])
H3 = np.array([-0.1267,0.12760,0.25440])*np.array([5.9677,5.9676,5.9676])
N2 = np.array([0.04,-0.038,-0.201])*np.array([5.9677,5.9676,5.9676])
H4 = np.array([0.1596,0.0408,-0.2186])*np.array([5.9677,5.9676,5.9676])
H5 = np.array([0.0008,-0.13270,-0.3016])*np.array([5.9677,5.9676,5.9676])


data_points = np.array([C,H1,N1,H2,H3,N2,H4,H5])
labels = ['C','H1','N1','H2','H3','N2','H4','H5']

print "\n"

print "The distance between C and N1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[2]))
print "The distance between C and N2 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[5]))
print "The distance between N1 and N2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[5]))
print "The distance between C and H1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[1]))
print "The distance between N1 and H2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[3]))
print "The distance between N1 and H3 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[4]))
print "The distance between N2 and H4 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[6]))
print "The distance between N2 and H5 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[7]))
f1 = plt.figure()

ax1 = f1.add_subplot(111)

for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax1.scatter(data_points[idx][0],data_points[idx][2], c = colour)

annotate2D(ax1, labels, np.transpose(np.array([data_points[:,0],data_points[:,2]])))

##rotating so that the N-N axis points in the x direction
data_points = data_points - data_points[2]
unit_N_N = 1.0/np.linalg.norm(data_points[2]-data_points[5])*(data_points[2]-data_points[5])

R = generate_rotation_vector(unit_N_N, np.array([-1,0,0], dtype = float))
data_points = apply_rotation(R, data_points)

print "\n"

print"After Rotation" + "\n"
print data_points

np.save('rotation1.npy',data_points)
print "The distance between C and N1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[2]))
print "The distance between C and N2 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[5]))
print "The distance between N1 and N2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[5]))
print "The distance between C and H1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[1]))
print "The distance between N1 and H2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[3]))
print "The distance between N1 and H3 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[4]))
print "The distance between N2 and H4 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[6]))
print "The distance between N2 and H5 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[7]))

f2 = plt.figure()
ax2 = f2.add_subplot(111)
for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax2.scatter(data_points[idx][0],data_points[idx][2], c = colour)
annotate2D(ax2, labels, np.transpose(np.array([data_points[:,0],data_points[:,2]])))

#Rotating so that the C-H plane is in the the x-z plane
data_points = data_points - data_points[0]
unit_C_H = 1.0/np.linalg.norm(data_points[0] - data_points[1])*(data_points[0] - data_points[1])
R_3 = generate_rotation_vector(unit_C_H,np.array([0,0,1], dtype = float))
data_points = apply_rotation(R_3,data_points)

print "\n"

print"After Rotation 2" + "\n"
print data_points
np.save('rotation2.npy',data_points)
print "The distance between C and N1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[2]))
print "The distance between C and N2 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[5]))
print "The distance between N1 and N2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[5]))
print "The distance between C and H1 is {0:.2f} A".format(np.linalg.norm(data_points[0] - data_points[1]))
print "The distance between N1 and H2 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[3]))
print "The distance between N1 and H3 is {0:.2f} A".format(np.linalg.norm(data_points[2] - data_points[4]))
print "The distance between N2 and H4 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[6]))
print "The distance between N2 and H5 is {0:.2f} A".format(np.linalg.norm(data_points[5] - data_points[7]))

f3 = plt.figure()
ax3 = f3.add_subplot(111)
for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax3.scatter(data_points[idx][0],data_points[idx][2], c = colour)
annotate2D(ax3, labels, np.transpose(np.array([data_points[:,0],data_points[:,2]])))



##Distances for rotation about C
C_pi_R= generate_rotation_about_axis([0,0,1], np.pi)
C_pi_data_points = apply_rotation(C_pi_R,data_points)
C_pi_d = np.linalg.norm(C_pi_data_points - data_points, axis = 1)


f4 = plt.figure()

ax4 = f4.add_subplot(111)

for idx, label in enumerate(labels):
    if re.findall('C',label):
        colour = 'k'
    elif re.findall('N',label):
        colour = 'b'
    elif re.findall('H',label):
        colour = 'r'
    else:
        colour = 'g'
    ax4.scatter(C_pi_data_points[idx][0],C_pi_data_points[idx][2], c = colour)

annotate2D(ax4,labels,np.transpose(np.array([C_pi_data_points[:,0],C_pi_data_points[:,2]])))


np.save('C2_rotation_distances.npy',C_pi_d)
plt.show()

pickle.dump(labels,open('labels.pickle','wb'))
