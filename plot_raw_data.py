# -*- coding: utf-8 -*-
"""
Created on Sun Mar 10 21:23:44 2019

@author: ak4jo
"""

import os
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
from scipy.special import jv
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import ticker, cm
sys.path.insert(0, '../../../')
import DataAnalysis as aj



# noqa: F401 unused import


working_directory = re.sub(r'\\',r'/',r'Sample_Bulk Perovskite/')
folders = [x + '/' for x in os.listdir(working_directory) if os.path.isdir(os.path.join(working_directory, x)) and x!= '.ipynb_checkpoints']

q = np.array([0.3,0.5,0.7,0.9,1.1,1.3,1.5,1.7,1.9])
for i,folder in enumerate(folders):
    print folder
    
    temps = [x + '/' for x in os.listdir(working_directory+folder) if os.path.isdir(os.path.join(working_directory+folder, x)) and x!= '.ipynb_checkpoints']
    
    if i==0:
        df = pd.DataFrame(index = folders, columns = [re.findall(r'\d+',x)[0] for x in temps], dtype = object)
    for idx, temp in enumerate(temps):
        print temp
        fit_data = working_directory + folder +temp + [x for x in os.listdir(working_directory+folder+temp) if re.findall(r'fits?\.txt',x)][0]
        fit_info = working_directory + folder +temp + [x for x in os.listdir(working_directory+folder+temp) if re.findall(r'\.fit',x)][0]
        
        with open(fit_info,'rb') as f:
            Data = f.read().splitlines()
            
        
        Curve_1_idx = [idx for idx,x in enumerate(Data) if re.findall('Curve 1',x)]
        Curve_2_idx = [idx for idx,x in enumerate(Data) if re.findall('Curve 2',x)]
        Curve_3_idx = [idx for idx,x in enumerate(Data) if re.findall('Curve 3',x)]
        Curve_4_idx = [idx for idx,x in enumerate(Data) if re.findall('Curve 4',x)]
        
        Curve_1 = re.findall(r'(Curve 1):\s+(.+)',Data[Curve_1_idx[0]])[0][1]
        Curve_2 = re.findall(r'(Curve 2):\s+(.+)',Data[Curve_2_idx[0]])[0][1]
        Curve_3 = re.findall(r'(Curve 3):\s+(.+)',Data[Curve_3_idx[0]])[0][1]
        Curve_4 = re.findall(r'(Curve 4):\s+(.+)',Data[Curve_4_idx[0]])[0][1]
        list_of_curves = [Curve_1, Curve_2, Curve_3, Curve_4]
        lor = [i for i, x in enumerate(list_of_curves) if x =='LORENTZIAN']
        info = {}

        for i,cur in enumerate([Curve_1_idx, Curve_2_idx, Curve_3_idx, Curve_4_idx]):
            if i in lor:
                FWHM = [re.findall(r'FWHM:\s+(\d+\.\d+e\+\d+)', Data[x+3]) for x in cur][0]
                area = [re.findall(r'area:\s+(\d+\.\d+e[\+|-]\d+)', Data[x+1]) for x in cur]
                info['Curve_'+str(i)] = [list_of_curves[i], FWHM, area]
            else:
                info['Curve_' + str(i)] = [list_of_curves[i]]
        
        with open(fit_data,'rb') as f:
            Data = f.read().splitlines()
        
        sep_idx = [idx for idx,x in enumerate(Data) if re.findall('Group',x)]
        
        sep_idx.append(len(Data))
        
        ##all data in order: E, data, err, total fit, curve 1, curve 2, curve 3, curve 4
        all_data = np.zeros([8, len(sep_idx)-1, sep_idx[1]-3])
        
        for idx, val in enumerate(sep_idx[:-1]):
            all_data[0,idx,:] = np.array([float(re.split('\s+',x)[1]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
            all_data[1,idx,:] = np.array([float(re.split('\s+',x)[2]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
            all_data[2,idx,:] = np.array([float(re.split('\s+',x)[3]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
            all_data[3,idx,:] = np.array([float(re.split('\s+',x)[4]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
            all_data[4,idx,:] = np.array([float(re.split('\s+',x)[5]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
            all_data[5,idx,:] = np.array([float(re.split('\s+',x)[6]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
            all_data[6,idx,:] = np.array([float(re.split('\s+',x)[7]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
            all_data[7,idx,:] = np.array([float(re.split('\s+',x)[8]) for x in Data[val+2:sep_idx[idx+1]-1]]).reshape(1, sep_idx[1]-3)
        

        f =plt.figure(figsize = [10,10])
        f.tight_layout()
        f.subplots_adjust(bottom = 0.05, top = 0.95, left = 0.05, right = 0.95)
        ax =  f.add_subplot(111,projection = '3d')
        
        colors = cm.Reds(np.linspace(0.4,1, len(sep_idx)-1))
        for q_group,en in enumerate(all_data[0]):
            start = np.argmin(np.abs(all_data[0,idx,:] + 10))
            end = np.argmin(np.abs(all_data[0,idx,:] - 10))
            ax.plot(en[start:end],np.ones(len(en[start:end]))*q[q_group], all_data[1,q_group,start:end], \
                    color = colors[q_group], linestyle = '-.', linewidth = 2)


        aj.figure_quality_axes(ax, 'Energy (meV)', r'Q ($\bf{A^{-1}}$)', '', legend = False)
        ax.set_zlabel('Intensity (A.U.)', weight = 'bold', fontsize = 20, linespacing = 3.2)
        ax.tick_params(axis='z',labelsize = 20, width = 3,length =10)
        ax.set_xlim([-10,10])
        ax.dist = 10
        f.suptitle(re.findall(r'\d+Br',folder)[0] + ' - ' + re.findall(r'\d+\s?K',temp)[0], weight = 'bold', fontsize = 20)
        df.loc[folder, re.findall(r'\d+',temp)[0]] = [all_data, info]
        f.savefig('Figures/Raw_data/' + re.findall(r'\d+Br',folder)[0] + ' - ' + re.findall(r'\d+\s?K',temp)[0]+'.svg', format = 'svg',dpi = 1200 )

df.to_pickle(open('All_Bulk_Data_Pickle','wb'))
