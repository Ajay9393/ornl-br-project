# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 11:12:06 2019

@author: ak4jo
"""

import numpy as np
import re
import pickle
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import matplotlib.lines as mlines
from matplotlib.pyplot import cm
from scipy.optimize import curve_fit
import itertools
from sklearn.metrics import r2_score
import pandas as pd

def fit_multiple_polys(list_of_functions):
    def sum_polys(x, *n1):
    ##n1 will be all fractions for MA rotations and all fraactions for FA rotations, in the order corresponding
    ##to the flattened array of polys passed (i.e all MA then all FA)
        
        MA_polyf = list_of_functions[0][:]
        FA_polyf = list_of_functions[1][:]
        
        num_MA = len(MA_polyf)
        num_FA = len(FA_polyf)
        temp = np.zeros(len(x))
        n1 = np.array(n1).ravel()
        for idx in range(0,num_MA):
            fcn = MA_polyf[idx]
            temp = temp + n1[idx]*(1.-fcn(x))
        for idx in range(0,num_FA):
            fcn = FA_polyf[idx]
            temp = temp + n1[idx + num_MA]*(1.-fcn(x))   

        return 1. - temp
    return sum_polys
    
        

EISF = pickle.load(open('EISF.pickle','rb'))

FA_C6_C_center_EISF = pickle.load(open('FA_C6_C_Center_EISF.pickle','rb'))
FA_C6_N_center_EISF = pickle.load(open('FA_C6_N_Center_EISF.pickle','rb'))
FA_C6_Centermass_center_EISF = pickle.load(open('FA_C6_Centermass_Center_EISF.pickle','rb'))
FA_C6_twirl_EISF = pickle.load(open('FA_C6_twirl_EISF.pickle','rb'))
#FA_C2_twirl_EISF = pickle.load(open('FA_C2_twirl_EISF.pickle','rb'))
MA_C3_C6_EISF = pickle.load(open('MA_total_EISF.pickle','rb'))
MA_C3_EISF = pickle.load(open('MA_C3_EISF.pickle','rb'))
MA_C6_EISF = pickle.load(open('MA_C6_EISF.pickle','rb'))

q = np.linspace(0,5,num=len(FA_C6_twirl_EISF))


FA_functions = {'FA_C6_C_Center':FA_C6_C_center_EISF,
                'FA_C6_N_Center':FA_C6_N_center_EISF,
                'FA_C6_Centermass_Center': FA_C6_Centermass_center_EISF,
                'FA_C6_Rotation': FA_C6_twirl_EISF}
                #'FA_C2_twirl': FA_C2_twirl_EISF}

MA_functions = {'MA_C3': MA_C3_EISF,
                'MA_C6': MA_C6_EISF,
                'MA_total': MA_C3_C6_EISF}


FA_poly = {}

MA_poly = {}

f, ax = plt.subplots(nrows =1, ncols = 2, figsize = (14,7), sharey = True)
f.subplots_adjust(wspace = 0.4)
color = cm.nipy_spectral(np.linspace(0,0.8,len(FA_functions.keys())))
legend_FA = []
legend_MA = []

for idx, key in enumerate(FA_functions.keys()):
    FA_poly[key] = np.poly1d(np.polyfit(q, FA_functions[key],15))
    ax[0].plot(q, FA_functions[key], c =color[idx], marker = 'o', markersize = '0.5')
    line, = ax[0].plot(q, FA_poly[key](q), c = color[idx],linestyle = '-.')
    legend_FA.append(mlines.Line2D([],[],color = color[idx], marker = 'o', label = key))

ax[0].legend(handles = legend_FA)

for idx, key in enumerate(MA_functions.keys()):
    MA_poly[key] = np.poly1d(np.polyfit(q, MA_functions[key],15))
    ax[1].plot(q, MA_functions[key], c =color[idx], marker = 'o', markersize = '0.5')
    line, = ax[1].plot(q, MA_poly[key](q), c = color[idx],linestyle = '-.')
    legend_MA.append(mlines.Line2D([],[],color = color[idx], marker = 'o', label = key))

ax[1].legend(handles = legend_MA)


bulk  = [x for x in EISF.keys() if re.findall('Bulk',x)]

lorentz = 'Narrow'
lorentz = 'Broad'
dict_to_fit = {x:EISF[x] for x in bulk if re.findall(lorentz,x)}

list_of_all_FA_modes =[(['FA_C6_N_Center']),(['FA_C6_Rotation']),(['FA_C6_C_Center']), (['FA_C6_Centermass_Center'])]

#list_of_all_FA_modes.insert(0, [])

#list_of_all_MA_modes = [x for l in range(1, len(MA_poly.keys())+1) for x in \
#                       itertools.combinations(MA_poly.keys(), l)]

list_of_all_MA_modes = [(['MA_C3']),(['MA_total']),[]]
#list_of_all_MA_modes = [[('MA_total')]]

identifiers = []
for FA in list_of_all_FA_modes:
    for MA in list_of_all_MA_modes:
        try:
            identifiers.append(str(FA[0]) + '+' + str(MA[0]))
        except IndexError:
            identifiers.append(str(FA[0]))
            
samples = sorted(dict_to_fit.keys(), key = lambda x: (int(re.findall(r'(\d+)\s?K',x)[0]), int(re.findall(r'(\d+)\s?Br',x)[0])))      
sample_name = [re.findall(r'\d+Br',x)[0] + '-' +re.findall(r'\d+\s?K',x)[0] for x in samples]

dict_of_fits=pd.DataFrame(index = identifiers, columns = sample_name)
dict_of_r2 = pd.DataFrame(index = identifiers, columns = sample_name)


color = cm.nipy_spectral(np.linspace(0,0.8,len(list_of_all_MA_modes)))
q = np.linspace(0,1.8,num=100)

for idx1, key in enumerate(samples):    
    f3, ax3 = plt.subplots(nrows = len(list_of_all_FA_modes)/2 + len(list_of_all_FA_modes)%2, \
                           ncols = 2, figsize = (20,40))
    f3.subplots_adjust(wspace = 0.4, top = 1)
    f3.tight_layout(rect=[0, 0.03, 1, 0.95])
    q_val = EISF[key][0][:]
    val = EISF[key][1][:]
    for idx2,combo in enumerate(list_of_all_FA_modes): 
        ax3[idx2 / 2, idx2 % 2].scatter(q_val[:], val[:], color = 'k', label = key)
        
        for idx3,combo2 in enumerate(list_of_all_MA_modes):
            
            identifier = []
            
            if combo2 != []:
                identifier.append('+'.join(combo2))
                
            if combo != []:
                identifier.append('+'.join(combo))

                
            if identifier ==[]:
                break
                
            
            
            MA_fcn = [MA_poly[x] for x in combo2]

            FA_fcn = [FA_poly[x] for x in combo]

            bounds = (np.zeros(len(FA_fcn + MA_fcn)), np.concatenate(([0.15*6./(0.8*5. + 0.15*6.)]*len(MA_fcn),[0.8*5./(0.8*5. + 0.15*6.)]*len(FA_fcn))))
            p0 = np.concatenate(([0.1]*len(MA_fcn), [0.5]*len(FA_fcn)))
            try:
                pp1, pp2 = curve_fit(fit_multiple_polys([MA_fcn, FA_fcn]),xdata = q_val,ydata = val, p0 =p0, bounds = bounds)
            except (RuntimeError, ValueError) as e:
                print e
                print "Some shit went wrong", identifier
                dict_of_fits.loc[identifiers[3*idx2+idx3],sample_name[idx1]] = [0,0]
                ax3[idx2 / 2, idx2 % 2].plot(0,0, label = '+'.join(identifier) + ' did not converege')
            else:
                y_pred = fit_multiple_polys([MA_fcn, FA_fcn])(q_val, *pp1)
                y_temp = fit_multiple_polys([MA_fcn, FA_fcn])(q, *pp1)
                r_squared = r2_score(val,y_pred)


                ax3[idx2 / 2, idx2 % 2].plot(q, y_temp, color = color[idx3], \
                   linestyle = '-.', label = '+'.join(combo2) + ': $R^{2}$ = ' + str(round(r_squared,3))\
                   + ', $x_1$ = ' + ','.join([str(round(x,6)) for x in pp1.tolist()]))
#                    print r_squared
               
                
                ax3[idx2 /2, idx2 % 2].set_title('FA modes ='+ '+'.join(combo))
#                    print len(combo) + len(combo2), pp1
            dict_of_fits.loc[identifiers[3*idx2+idx3],sample_name[idx1]] = [pp1, r_squared]
            dict_of_r2.loc[identifiers[3*idx2+idx3],sample_name[idx1]] = round(r_squared,3)
            handles, labels = ax3[idx2/2, idx2%2].get_legend_handles_labels()
            ax3[idx2 / 2, idx2 % 2].legend(handles, labels)
        
#    
    f3.suptitle(key)
    
    f3.savefig('Sample_Bulk Perovskite/'+re.sub(r'/','_',key)[0:len(key) - 4] + '.svg',dpi = 1200)
    plt.close(f3)
  

dict_of_fits.to_csv('All_Fits' + lorentz + '.csv')  
dict_of_r2.to_csv('All_R2' + lorentz + '.csv')      
#pickle.dump(dict_of_fits, open('all_fits_' + lorentz + '.pickle','wb'))
#
#temp = dict_of_fits.T
#
#temp.to_csv('All_Fits' + lorentz + '.csv')

#for key in sorted(dict_of_fits.index, key = lambda x: (int(re.findall(r'(\d+)\s?K',x)[0]), int(re.findall(r'(\d+)\s?Br',x)[0]))):
#    print '\n' + '\n'
#    print "The best fit for {0}, with an r-squraed of {1}, is: ".format(key,round(dict_of_fits.loc[key,[0][1][1],4)) +'\n'
#    print "{0:25} {1:^25}".format("Mode:", "Active h-Fraction:")
#    for idx, mode in enumerate(dict_of_fits[key][0][0].split('+')):
#        print "{0:25} {1:^25}".format(mode, dict_of_fits[key][0][1][0][idx])
        
    