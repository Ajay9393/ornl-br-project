# -*- coding: utf-8 -*-
"""
Created on Wed Sep 11 10:30:05 2019

@author: ak4jo
"""

import numpy as np
from scipy.signal import unit_impulse
import pandas as pd
import re
import matplotlib.pyplot as plt
import os
import sys
sys.path.insert(0, '../../')
import DataAnalysis as aj
from scipy.signal import gaussian
import scipy.stats as stats
from scipy.optimize import curve_fit
from sklearn.metrics import r2_score


def gf(x,x0,gam):
    
    y= np.exp(-0.5*((x - x0)/gam)**2)/np.sqrt(2.*np.pi)/gam
    return y/np.max(y)
    
 
def elliot_a2(E_i,Eb, Eg,g):
    num_n=1000
    s = np.min(E_i)
    e = np.max(E_i)

    
    E = np.linspace(s,e+5, 5000)
    
    Ebar = (E - Eg)/Eb
    
    temp = np.zeros(len(E))
    
    for i in range(1, num_n+1):
        temp += g/float(i)**3./(np.square(Ebar + 1./float(i)**2.) + g**2.) 
    
    temp = temp*4.

    Se = 2*np.pi/(np.sqrt(Ebar)*(1. - np.exp(-2.*np.pi/np.sqrt(Ebar))))*g*np.sqrt(Ebar)
    Se[np.isnan(Se)]  = 0
    y = np.zeros(len(Ebar))
    for idx,e in enumerate(Ebar):
        y[idx] += np.trapz(Se/np.pi/(np.square(e - Ebar) + np.square(g)), Ebar)
        
    temp+=y
    
    temp = np.interp(E_i, E, temp)
    
    
    return temp/np.max(temp)
    
    
E = np.linspace(1.3,1.7,1000)
Eg = 1.55

Eb = np.linspace(1e-3, 20e-3, 30)
g = 1000e-3
num_n=1000
A = 1
f_t, ax_t = plt.subplots(nrows = 1, ncols = 1)

#alph = elliot_a(E, A, Eb, Eg, num_n, g)
for e in Eb:    
    alph2 = elliot_a2(E, e, Eg,g)

    ax_t.plot( E, alph2)


wd = r'Br-Study/SC Low T Abs/'

files = os.listdir(wd)

files = [x for x in files if re.findall(r'1s',x)]

w_b, c_b = aj.low_T_PL(wd+'blank-1s.txt')
w_900 = aj.find_nearest(900, w_b)

e = aj.find_nearest(1000, w_b)
s = aj.find_nearest(670, w_b)
h = 6.626e-34
v=3.0e8


f, ax = plt.subplots(nrows = 2, ncols = 2, figsize = (15,15))

f.tight_layout()
f.subplots_adjust(wspace = 0.25, hspace = 0.25)


files = [x for x in files if re.findall('Br',x)]
files.sort(key = lambda x: (int(re.findall(r'(\d+)Br',x)[0]), int(re.findall(r'(\d+)K',x)[0])))


fit = 0
all_fits = pd.DataFrame(index = files, columns = ['Binding Energy','Band Gap','Broadening','Binding Energy var','Band Gap var','Broadening var','r2'])

for i, x in enumerate(sorted([x for x in files if re.findall('0Br',x) and not re.findall('10Br',x) and not re.findall('20Br',x) ], \
                              key = lambda x: int(re.findall(r'(\d+)K',x)[0]))):
    w, c = aj.low_T_PL(wd + x)
    E = h*v/w[s:e]/1.6e-19*1e9
    c = c_b[w_900]/ c[w_900]*c
    y = aj.min_max(c_b[s:e] - c[s:e])
    ax[0,0].plot(E,y,label = re.findall(r'(\d+)K',x)[0] + ' K')
    if fit:
        bounds = ([5e-3,1.43,0],[1,1.53,5])
        popt, pvar = curve_fit(elliot_a2, E, y, p0 = [15e-3,1.46,0.8],)
        
        y_pred = elliot_a2(E, *popt)
        
        r2 = r2_score(y, y_pred)
        print popt
        print r2
        all_fits.loc[x, 'Binding Energy':'Broadening'] = popt
        all_fits.loc[x, 'Binding Energy var':'Broadening var'] = np.sqrt(np.diag(pvar))
        all_fits.loc[x, 'r2'] = r2
        
        ax[0,0].plot(E, y_pred, linewidth = 1, linestyle = '--', color = 'k')

aj.figure_quality_axes(ax[0,0], 'Energy (eV)','Absorption (A.U)','0% Br', legend = True)
    

for i, x in enumerate(sorted([x for x in files if re.findall('10Br',x)], \
                              key = lambda x: int(re.findall(r'(\d+)K',x)[0]))):
    w, c = aj.low_T_PL(wd + x)
    E = h*v/w[s:e]/1.6e-19*1e9
    c = c_b[w_900]/ c[w_900]*c
    y = aj.min_max(c_b[s:e] - c[s:e])
    ax[0,1].plot(E,y,label = re.findall(r'(\d+)K',x)[0] + ' K')
    if fit:
        bounds = ([5e-3,1.5,0],[1,1.56,5])
        popt, pvar = curve_fit(elliot_a2, E, y, p0 = [15e-3,1.51,0.8], )
        
        y_pred = elliot_a2(E, *popt)
        
        r2 = r2_score(y, y_pred)
        print popt
        print r2
        all_fits.loc[x, 'Binding Energy':'Broadening'] = popt
        all_fits.loc[x, 'Binding Energy var':'Broadening var'] = np.sqrt(np.diag(pvar))
        all_fits.loc[x, 'r2'] = r2
        
        ax[0,1].plot(E, y_pred, linewidth = 1, linestyle = '--', color = 'k')
    
    

aj.figure_quality_axes(ax[0,1], 'Energy (eV)','Absorption (A.U)','10% Br', legend = True)

for i, x in enumerate(sorted([x for x in files if re.findall('15Br',x)], \
                              key = lambda x: int(re.findall(r'(\d+)K',x)[0]))):
    w, c = aj.low_T_PL(wd + x)
    E = h*v/w[s:e]/1.6e-19*1e9
    c = c_b[w_900]/ c[w_900]*c
    y = aj.min_max(c_b[s:e] - c[s:e])
    ax[1,0].plot(E,y,label = re.findall(r'(\d+)K',x)[0] + ' K')
    if fit:
        bounds = ([5e-3,1.55,0],[1,1.65,5])
        popt, pvar = curve_fit(elliot_a2, E, y, p0 = [15e-3,1.6,0.8], )
        
        y_pred = elliot_a2(E, *popt)
        
        r2 = r2_score(y, y_pred)
        print popt
        print r2
        all_fits.loc[x, 'Binding Energy':'Broadening'] = popt
        all_fits.loc[x, 'Binding Energy var':'Broadening var'] = np.sqrt(np.diag(pvar))
        all_fits.loc[x, 'r2'] = r2
        
        ax[1,0].plot(E, y_pred, linewidth = 1, linestyle = '--', color = 'k')
    

aj.figure_quality_axes(ax[1,0], 'Energy (eV)','Absorption (A.U)','15% Br', legend = True)

for i, x in enumerate(sorted([x for x in files if re.findall('20Br',x)], \
                              key = lambda x: int(re.findall(r'(\d+)K',x)[0]))):
    
    w, c = aj.low_T_PL(wd + x)
    E = h*v/w[s:e]/1.6e-19*1e9
    c = c_b[w_900]/ c[w_900]*c
    y = aj.min_max(c_b[s:e] - c[s:e])
    ax[1,1].plot(E,y,label = re.findall(r'(\d+)K',x)[0] + ' K')
    if fit:
        bounds = ([5e-3,1.58,0],[1,1.7,5])
        popt, pvar = curve_fit(elliot_a2, E, y, p0 = [15e-3,1.6,0.8], )
        
        y_pred = elliot_a2(E, *popt)
        
        r2 = r2_score(y, y_pred)
        print popt
        print r2
        all_fits.loc[x, 'Binding Energy':'Broadening'] = popt
        all_fits.loc[x, 'Binding Energy var':'Broadening var'] = np.sqrt(np.diag(pvar))
        all_fits.loc[x, 'r2'] = r2
        
        ax[1,1].plot(E, y_pred, linewidth = 1, linestyle = '--', color = 'k')
    

aj.figure_quality_axes(ax[1,1], 'Energy (eV)','Absorption (A.U)','20% Br', legend = True)  

f.savefig('Low Temperature Absorption.svg',format = 'svg', dpi=1000)    



